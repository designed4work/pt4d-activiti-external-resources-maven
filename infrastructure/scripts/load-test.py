# -*- coding: utf-8 -*-

import urlfetch
import os
import time
import optparse
import sys
import traceback
import base64
import json
import urllib2
import threading
import time
import logging

from poster.encode import MultipartParam, multipart_encode

ACTIVITI_URL = 'http://35.186.202.73/'
ACTIVITI_ADMIN_USER = '109799398936832035014'
ACTIVITI_ADMIN_PASSWORD = 'password'

CREATE_NEW_DEPLOYMENT = dict(
    name='CREATE_NEW_DEPLOYMENT',
    url='/activiti-rest/service/repository/deployments',
    method='POST',
)
START_PROCESS_INSTANCE = dict(
    name='START_PROCESS_INSTANCE',
    url='/activiti-rest/service/runtime/process-instances',
    method='POST',
)
LIST_PROCESS_DEPLOYMENTS = dict(
    name='LIST_PROCESS_DEPLOYMENTS',
    url='/activiti-rest/service/repository/deployments',
    method='GET',
)
LIST_PROCESS_DEFINITIONS = dict(
    name='LIST_PROCESS_DEFINITIONS',
    url='/activiti-rest/service/repository/process-definitions',
    method='GET',
)
LIST_PROCESS_INSTANCES = dict(
    name='LIST_PROCESS_INSTANCES',
    url='/activiti-rest/service/runtime/process-instances',
    method='GET',
)
LIST_HISTORY_PROCESS_INSTANCES = dict(
    name='LIST_HISTORY_PROCESS_INSTANCES',
    url='/activiti-rest/service/history/historic-process-instances',
    method='GET',
)
LIST_PROCESS_INSTANCES_VARIABLES = dict(
    name='LIST_PROCESS_INSTANCES_VARIABLES',
    url='/activiti-rest/service/runtime/process-instances/{}/variables',
    method='GET',
)
LIST_TASKS = dict(
    name='LIST_TASKS',
    url='/activiti-rest/service/runtime/tasks',
    method='GET',
)
LIST_HISTORY_TASKS = dict(
    name='LIST_HISTORY_TASKS',
    url='/activiti-rest/service/history/historic-task-instances',
    method='GET',
)
TASK_ACTION = dict(
    name='TASK_ACTION',
    url='/activiti-rest/service/runtime/tasks/{}',
    method='POST',
)
GET_TASK_VARIABLES = dict(
    name='GET_TASK',
    url='/activiti-rest/service/runtime/tasks/{}/variables?scope=global',
    method='GET',
)

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

DOMAINS = ["default"]
#DOMAINS = ["default", "domain1-com", "domain2-com", "domain3-com", "domain4-com", "domain5-com", "domain6-com", "domain7-com", "domain8-com", "domain9-com"]

IS_LOCAL_HOST = False
PROCESS_DEFINITION_KEY = "load-test"
PROCESS_DEFINITION_FILE = "/Users/stephane/eclipse_workspace/pt4d-activiti-test/src/test/java/com/pt4d/LoadTest.bpmn"
PROCESS_DEFINITION_FILE_NAME = PROCESS_DEFINITION_KEY + ".bpmn"
START_USER = "admin@devcirruseo.com"
COMPLETE_USER = "user1@devcirruseo.com"


def call(call_type, slug_domain, username=ACTIVITI_ADMIN_USER, password=ACTIVITI_ADMIN_PASSWORD, in_url_id=None, payload=None, params=None, headers={}, size=100, start=None, sort=None, order=None):
    default_header = {
        'Authorization': 'Basic {}'.format(base64.b64encode('%s:%s' % (username, password))),
        'Content-Type': 'application/json'
    }
    default_header.update(headers)


    if not IS_LOCAL_HOST:
        url = ACTIVITI_URL + slug_domain + call_type.get('url')
    else:
        url = "http://localhost:9090" + call_type.get('url')
    
    if in_url_id:
        url = url.format(in_url_id)

    url += '?size={}'.format(size) if '?' not in  url else '&size={}'.format(size)
    if start:
        url += '&start={}'.format(start)
    if sort:
        url += '&sort={}'.format(sort)
    if order:
        url += '&order={}'.format(order)

    if params:
        for key, value in params.iteritems():
            url += '&{}={}'.format(key, value)

    response = urlfetch.fetch(
        url=url,
        method=call_type.get('method', None),
        headers=default_header,
        data=payload
    )

    content = None
    try:
        content = json.loads(response.content)
    except:
        logging.info("Cannot parse Content {}".format(response.content))

    logging.info("Calling activiti {} ({} / {}) : {} {}".format(call_type.get("name"), response.status, response.reason, call_type.get("method"), url))
    if not str(response.status).startswith('2'):
        logging.info(response.content)

    return content

def is_process_deployed(domain):
    content = call(LIST_PROCESS_DEFINITIONS, slug_domain=domain)
    for process_def in content.get('data'):
        if process_def.get('key') == PROCESS_DEFINITION_KEY:
            return True
    return False

def deploy_process(domain):
    with open(PROCESS_DEFINITION_FILE, 'r') as myfile:
        string_file = myfile.read()

    if not IS_LOCAL_HOST:
        url = ACTIVITI_URL + domain + CREATE_NEW_DEPLOYMENT.get('url')
    else:
        url = "http://localhost:9090" + CREATE_NEW_DEPLOYMENT.get('url')

    payload = [MultipartParam(
            PROCESS_DEFINITION_FILE_NAME,
            filename=PROCESS_DEFINITION_FILE_NAME,
            filetype="application/xml",
            value=string_file
        )]
    data, headers = multipart_encode(payload)

    headers.update({
        'Authorization': 'Basic {}'.format(base64.b64encode('%s:%s' % (ACTIVITI_ADMIN_USER, ACTIVITI_ADMIN_PASSWORD)))
    })
    request = urllib2.Request(url, "".join(data), headers)
    
    logging.info("Process deployed for {} on {}".format(domain, url))
    urllib2.urlopen(request)

def start_process_instance(domain, file_id):
    logging.info("Start process for file {}".format(file_id))
    payload = {
       "processDefinitionKey":PROCESS_DEFINITION_KEY,
       "variables": [
          {
            "name":"file_id",
            "value":file_id,
          }
       ]
    } 
    return call(call_type=START_PROCESS_INSTANCE, slug_domain=domain, payload=json.dumps(payload))

def list_user_task(domain, user_id):
    return call(call_type=LIST_TASKS, slug_domain=domain, params=dict(candidateUser=user_id))

def complete_task(domain, task_id, variables=[]):
    payload = {
       "action" : "complete",
       "variables": variables
    }
    return call(call_type=TASK_ACTION, slug_domain=domain, in_url_id=task_id, payload=json.dumps(payload))

def get_task_variables(domain, task_id):
    return call(call_type=GET_TASK_VARIABLES, slug_domain=domain, in_url_id=task_id)

def worker_check_task(domain):
    logging.info("Starting worker_check_task for {}".format(domain))
    task_number = 0
    while True:
        content = list_user_task(domain, COMPLETE_USER)
        tasks = content.get('data')

        for task in tasks:
            variables = get_task_variables(domain, task.get('id'))
            logging.info(variables)
            response = complete_task(domain, task.get('id'), [dict(name="task_number", value=task_number)])
            task_number = task_number + 1

        time.sleep(1)

        if task_number >= len(FILE_IDS):
            break
    logging.info("Ending worker_check_task for {}".format(domain))

def worker_start_process(domain):
    logging.info("Starting worker_start_process for {}".format(domain))
    for file_id in FILE_IDS:
        if not is_process_deployed(domain):
            logging.info("Deploying process for {}".format(domain))
            deploy_process(domain)
        start_process_instance(domain, file_id)
        time.sleep(2)
    logging.info("Ending worker_start_process for {}".format(domain))

def main ():
    logging.getLogger().setLevel(logging.INFO)

    for domain in DOMAINS:
        #deploy_process(domain)
        #w1 = threading.Thread(name=domain+'_start_process', target=worker_start_process, args=[domain])
        w2 = threading.Thread(name=domain+'_check_task', target=worker_check_task, args=[domain])

        #w1.start()
        w2.start()




    # for domain in DOMAINS:
    #     if not is_process_deployed(domain):
    #         print "Deploying process"
    #         deploy_process(domain)

    #     for file_id in FILE_IDS:
    #         start_process_instance(domain, file_id)

    #     content = list_user_task(domain, COMPLETE_USER)
    #     tasks = content.get('data')

    #     for task in tasks:
    #         response = complete_task(domain, task.get('id'))

FILE_IDS = ["1dhE7sCUBskfC53XghUGbFv92FI6F3jb4cC4NtebIl5w","0B3qX-JV-O-RgdGhKTVBkYVdyWnM","1wtSI_BxCIqOBr8JhQAu1G5zm9HTkp_K6FANVpeTq53w","0B_WP6BwmjHqFMHkxN2VRRU5IZEE","1fjyVZDDPj3YPOIT0_5uqxpFxluIGgLpCT9ggkMaNwf8","19ce9MWxEp8UhX1Zn5GhlrgikMp1lqeiFxdRHlAUdx1w","1IV8Dvv1LhBh6-JVoUyO7qjnxBs6PWAEtAfXky7bYp5M","0B_WP6BwmjHqFUnk4Ym9sRU9pc1U","16H5eSooZSxqOMFicCtkuqLH7rPmC-J8rCyCfu7dHBTo","18pM6lvH3GfLib-rvMNASPr0OoFc46-7uAKfgH1GAEdk","1Aw26nErYFzAQOr6dzLiaUJiPfdD3rg_pPg2mYAh_O1U",
"1Gt0950P6dgps-xLMWL4eertJiDyqCYPJbg_Tj5VsfZQ","1QXI5UWdT9L0X1FicyEYOHngIyxiVLu82LZjAEy4gwGA","1ROgD2MxJFN_U8eByg1IKW-uiw-Pcu-7R6tdzsIhcHYI","1ZxmaakmEuaTZ2597sTqcNwbqx8Gee2lKQfyfqDDfiag","1oBpQ_XuBxpGnP-WNCepT6uywv8LnjAgzSOWw03PAtcY","1w8ufzBH-z9-UyP_gsFJY99w4NSwzmKU1VjEDEJw2rTk","1wPc2l944z35_a4eTIJU06jV1xvnd3yIDm9keUGIMf1I","1xpuEVaDNJR_2Vj7X_mwnbWPv3GAb-P45Tv6BMiAGvWY","0B3qX-JV-O-RgMExJelhNOUVncW8","0B_WP6BwmjHqFNFRBYWsyVE5IU1E","0B_WP6BwmjHqFSlpMWEUtb0E2V0k","0B_WP6BwmjHqFflBOQVBoQWEwb1ZOMHZZN2VoTkd0aTJhc1U0M1llMTJ5WnZ6OVNVQkl6WlE","13_9aux2Rx8LbY1baB8MthFkweBB593-OfqfLOPtqvnI","1C9aEJgoz2Vsgtwz2f5GFp3MKXBG1mnsNQIs1gUNrFJc","1PPD2JJm-T88bIknIL5dA0kUBgwnBSomwNL-kpX4yCzk","1UGxaaEIWZ7e4iRzA9yuGz4UhoUgi7mUO7E1hENt9WqA","1rWXyJTgj9EuN3tBEaFeXXhOuyiQpTAWIxs_vRrsGOOk","1Rp0S4wtPCQoFBPuuttiiGMDu4frrkYUzB7mjpL2wRUo","1WvTj-eZ5RxlpAgRYZwX3LwRFBsRUmKGcJutFzPeGEaE","0B3qX-JV-O-Rgc3JHd3I4QjFOdjQ","164UZh4VJaSVqyzmSFdt8QTOwdrY7o3Tn73zkPHvWbhU","0B3qX-JV-O-RgdUxBMGxRR3Bycm8","1GiZd7VUWEJzOHnR33uxs7ad1n25y4g4R101WxuzGwrQ","0B2duDe2NHBlkZjJoX1dNX0h3Y1U","0B3qX-JV-O-RgZHBxWVJISVBMQ28","0B_WP6BwmjHqFOGFOSXRYckdndGs","0B_WP6BwmjHqFV1ljYkdMTHFfZW8","0B_WP6BwmjHqFcnZ0Y1FzREZWaUk","12Sz0AAZceLp6rXLlbsBWZx4b4rbZy8sijUcQELB_T8g","14ZrfHMtbY3brbyXTNaWSmx6iU3A9J0fPlB1dIX6ArLk","1BjC0udpk1FjZ8I9Aw2qMDr8_9i9VYg2ayeMiP4IatC4","0B3qX-JV-O-RgfnVDd0JwOEVHakZPamJobXRhUkJXTmVzS096X0E1NEhKSEtRdzVoXzhLZEk","0B_WP6BwmjHqFSkxwY3dqOS1JZnM","0B2duDe2NHBlkZ2dFWXVYUXBmUVU","0B3qX-JV-O-Rgb1hKSVJEQnZWZTA","0B_WP6BwmjHqFN2pJejB5YWZzM2c","0B_WP6BwmjHqFNm9uUDE1Tjg5ZVk","0B_WP6BwmjHqFaFhjcDRvSnU5OHM","0B_WP6BwmjHqFdS1kMUsyWTdBOEE","1RmS1MUTbiVD87Fu2HEwyZ2j1YWIwTSs4gdVOTPvWCUo","1XZWG46iGROasy3wHjkaxI_NAskrP9lC11riAo7mrZSA","1gP1x-QI_MgXueLLDLvGRnEm_JrzQXcPpXGaQcVADahM","1M-rLiwHm_ySBF0GSpxvTXQlzqgh_5qEb5sFQwFhOsZk","1GpRTNyNqANxn9sgoy1V0OXF5m7j5uAzuBeshrSePARU","0B3qX-JV-O-RgLUFMU2x4RVF2Ync","0B3qX-JV-O-RgRmxFeTdzOFA5X0U","0B3qX-JV-O-RgUklRYURBc01xd1U","0B3qX-JV-O-RgaXY3WkdZOTI2LVE","1ITBTG1QJZMTHM7ixvkegLb5QQSofe2OIbelwCNWMqe4","1XWH8O3xXISm8rXqP5sioG4lnuppuUsw_HuIiGyskarw","1XA39G7YZBO4LhtkruCKCoXx3A4IWjFWzDkTr-7NQuIs","1XDBP0LPF52Z29SN8pKLX8Vfz1aO1Ft38s3FKqa2_9gI","1iokHYzyblHCMUtLkkaPRmpqxCWruOstdC9iMW38qkus","1m5gKCxtoZd1bi74b5zz5V0VWhHKhaNwx882wqA35Hu0","0B_WP6BwmjHqFT2NYV0FXbTE4RDQ","0B_WP6BwmjHqFTmV3cHVsMkpXYms","0B_WP6BwmjHqFa1lUay1ZRFVqX2s","1-tgtjwhOtWmT_JU32yTRBr9QUaT54WrjrYPQ9slZ2nI","18fHTEMvVqiQPUBQ1wbWo3mMltjkMgtD7VGp14w75kdI","0B2duDe2NHBlkLTBxWGNvd3dMY00","0B2duDe2NHBlkLTFpU0FfYk1RX28","0B2duDe2NHBlkLTNHbUJCMm9QQms","0B2duDe2NHBlkLU11dDh5Zlk1VW8","0B2duDe2NHBlkLU1fZlVJVGZVdE0","0B2duDe2NHBlkLUdib1F6QUZMcmc","0B2duDe2NHBlkLV8xQmNXbjN6RUk","0B2duDe2NHBlkLVBwWnlTdnVpRDA","0B2duDe2NHBlkLVNOOGxYMV9GUjg","0B2duDe2NHBlkLVRFaWpXWTRDMVU","0B2duDe2NHBlkLWc0NEdmRVlWMlk","0B2duDe2NHBlkLWdkR2ozX2tVbUU","0B2duDe2NHBlkLWowLXFKUHhqdzA","0B2duDe2NHBlkLXExSU81WXBGdUU","0B2duDe2NHBlkLXlpWWM4VkEyZVk","0B2duDe2NHBlkM083Q0paOGhGbk0","0B2duDe2NHBlkM0JIUVQwX0I5NkE","0B2duDe2NHBlkM0JkSDNsSlVEdm8","0B2duDe2NHBlkM0RRUC02eFh5Z1U","0B2duDe2NHBlkM0gwWXpHTXNlUXM","0B2duDe2NHBlkM0xuaVl3YXp1dUE","0B2duDe2NHBlkM1Bqd2p0dk50azA","0B2duDe2NHBlkM1dsczlXdXNwWjg","0B2duDe2NHBlkM1oxOGNDOU5aY1k","0B2duDe2NHBlkM1pvSE42ZDNmVUE","0B2duDe2NHBlkM2IzS2NCajR3LWc","0B2duDe2NHBlkM2RRRmEyM3c1c28","0B2duDe2NHBlkM2VEVkdpR3FqdU0","0B2duDe2NHBlkM3ItY0E2N0F3VjQ","0B2duDe2NHBlkM3V0a0pKbzI4b2c","0B2duDe2NHBlkMDE2SXBnVm1UU1k","0B2duDe2NHBlkMDhVQnZFQUw5NU0","0B2duDe2NHBlkMDhyX3MzUGR3eE0","0B2duDe2NHBlkMEVZTzF6X0FrUE0","0B2duDe2NHBlkMEZLUHlnbkRaWnc","0B2duDe2NHBlkMFQ3bzRSbUJQc28","0B2duDe2NHBlkMFZ4UVlZRy1nb1k","0B2duDe2NHBlkMFo3U2Ftb1FLcUU","0B2duDe2NHBlkMFpoVjFpRC1vWDQ","0B2duDe2NHBlkMGZCWFAwbExrdGc","0B2duDe2NHBlkMHBTYjRmRmI2dVU","0B2duDe2NHBlkMHRtZ1lFbjhtY2s","0B2duDe2NHBlkMHYwVFFIWnAyY28","0B2duDe2NHBlkMTBnWXhhc0l1eHc","0B2duDe2NHBlkMTFiM2JWenZ0OEU","0B2duDe2NHBlkMTlDajNZbzNSYWc","0B2duDe2NHBlkMU9fSWRtZVViUzA","0B2duDe2NHBlkMUJqYUdCcVRsV1k","0B2duDe2NHBlkMUNjdE9DbDhBM28","0B2duDe2NHBlkMUVSbVVCMjhJdTg","0B2duDe2NHBlkMUhHT0JBdnJkQjA","0B2duDe2NHBlkMUhxYUh6MG9adWc","0B2duDe2NHBlkMVM0WkhjYWdRQUU","0B2duDe2NHBlkMWZHbGNIa3p0eHc","0B2duDe2NHBlkMWdLN3oxajRfWWs","0B2duDe2NHBlkMWl2ck4tUllaek0","0B2duDe2NHBlkMXNuTHJTM2dRamc","0B2duDe2NHBlkMXRQazBKSVp0bG8","0B2duDe2NHBlkMXZSODNsVnd3ZHM","0B2duDe2NHBlkMjRrOFA1NExrQ3M","0B2duDe2NHBlkMk9NTTlkZl9adTQ","0B2duDe2NHBlkMkFqaVMzY0VWam8","0B2duDe2NHBlkMkFxWUN4N2JMRTg","0B2duDe2NHBlkMkRXTHZTMlM5M3c","0B2duDe2NHBlkMkZJLUI0LVA1Vlk","0B2duDe2NHBlkMkloRmZWZHhvUDQ","0B2duDe2NHBlkMl81dE5nbktKaHc","0B2duDe2NHBlkMlBPd3UyNVNMR0E","0B2duDe2NHBlkMlN3eWdFalhmZlk","0B2duDe2NHBlkMlU5TzV5dVdzTXM","0B2duDe2NHBlkMldvZTBaSXh6cWc","0B2duDe2NHBlkMllFM3JMZlhoakk","0B2duDe2NHBlkMlpVX0Y4SGVOQTQ","0B2duDe2NHBlkMnB5VTBCejdaQ2M","0B2duDe2NHBlkMnZjbURKeHl1Ulk","0B2duDe2NHBlkMndjeHNQeDZiRjg","0B2duDe2NHBlkN09JWWxHTlZwa0U","0B2duDe2NHBlkN0ZrR05oTUFxakE","0B2duDe2NHBlkN1BpVkFaeW15N0E","0B2duDe2NHBlkN29CYWNIMHV1UXM","0B2duDe2NHBlkN2J4bkRFMXdTdFE","0B2duDe2NHBlkN2dGVDBKa2NfeVk","0B2duDe2NHBlkN2djYkJpcTFYZXM","0B2duDe2NHBlkN3RrNFEzcGZTRUk","0B2duDe2NHBlkN3ZVa2VTSU5fSWM","0B2duDe2NHBlkN3dQbUpGbVo1UHc","0B2duDe2NHBlkN3hYdXNUeTBxSG8","0B2duDe2NHBlkNC05VmJucjhVWUk","0B2duDe2NHBlkNEVrc3huaEtkZmM","0B2duDe2NHBlkNEcxNTgtSWdqa2s","0B2duDe2NHBlkNF9rT2VnSTlncTQ","0B2duDe2NHBlkNFFxLXJTNm5KVjA","0B2duDe2NHBlkNFVIZXV3OEI5T0U","0B2duDe2NHBlkNFd6SHYwcDdTX0E","0B2duDe2NHBlkNG1LY2NPMTNHTWM","0B2duDe2NHBlkNG1NaTFlYmhVVG8","0B2duDe2NHBlkNGE0SlpYT2gyeTQ","0B2duDe2NHBlkNGNpampWbDdiQjQ","0B2duDe2NHBlkNGRaOWZILXp2dDQ","0B2duDe2NHBlkNHVOYjBzZnRDM0U","0B2duDe2NHBlkNHlDVkpyMmxFMkk","0B2duDe2NHBlkNUtFMFA0TUtfZXc","0B2duDe2NHBlkNVRJVG9lbmhfdHM","0B2duDe2NHBlkNVdmM0Z4NG5ab2c","0B2duDe2NHBlkNVpEZTIwaDFxT1k","0B2duDe2NHBlkNWI3LWh1YzJPNDg","0B2duDe2NHBlkNWZsRVlpdnF0Tlk","0B2duDe2NHBlkNXU2SU8xMHpBSlU","0B2duDe2NHBlkNXlFMDN2QU55eGc","0B2duDe2NHBlkNXpRZU1Vbnlkb0k","0B2duDe2NHBlkNjZlQkY2VndrekE","0B2duDe2NHBlkNjd4U295NUwzN2s","0B2duDe2NHBlkNk1ENjhWYXZqbVE","0B2duDe2NHBlkNk1kRHRlcUhjaTA","0B2duDe2NHBlkNkNGMkdGZ0ZHbEU","0B2duDe2NHBlkNlFmdzVTV09kQzA","0B2duDe2NHBlkNlhiaVNnUnItTXM","0B2duDe2NHBlkNm1keThOV1VHNjQ","0B2duDe2NHBlkNm9jbVNaM05pM1k","0B2duDe2NHBlkNmZidFY4VmhyV0U","0B2duDe2NHBlkNmczNGtwdnYyQm8","0B2duDe2NHBlkNmtQU1cxMm5EelU","0B2duDe2NHBlkNnBFbF9hSGFoUWM","0B2duDe2NHBlkNnZRWDc3RE5CODQ","0B2duDe2NHBlkNnZWVktFOWJlOHM","0B2duDe2NHBlkNzlYUEFnQTFYZGc","0B2duDe2NHBlkODQ4c09JY19ySW8","0B2duDe2NHBlkODViQzVLbFFVNXM","0B2duDe2NHBlkODgxTzZJbG5aNk0","0B2duDe2NHBlkOE85NzZsUzhBeGM","0B2duDe2NHBlkOEFfeUZneFkxY28","0B2duDe2NHBlkOEU4UFJCUXdRbTQ","0B2duDe2NHBlkOEVTM1FVdFM0UzA","0B2duDe2NHBlkOEVxOUQxRm1MQlU","0B2duDe2NHBlkOEc3NEg1bGVCSFU","0B2duDe2NHBlkOFhmcXB4WE43Uk0","0B2duDe2NHBlkOFlkYXhVRklaNHM","0B2duDe2NHBlkOGlUNWlXYWVIMUE","0B2duDe2NHBlkOHhuTTgzSHBGUm8","0B2duDe2NHBlkOTF5VGM4N3N5NlU","0B2duDe2NHBlkOU45VHR4RzZlQXM","0B2duDe2NHBlkOU81M0lPaVBoT1U","0B2duDe2NHBlkOU90OTF0NnJzZ00","0B2duDe2NHBlkOUZnS2Rqbkk4M1E","0B2duDe2NHBlkOUpJTW9UT3Q2b0U","0B2duDe2NHBlkOVBBeHlxSnFPVTQ","0B2duDe2NHBlkOVIxTnBqSE1LQVU","0B2duDe2NHBlkOVNRWXJlNHRqUGc","0B2duDe2NHBlkOVRpdWNoaTFhSDQ","0B2duDe2NHBlkOVhDMjFZZmstMW8","0B2duDe2NHBlkOW9MXzI0cFpHY0E","0B2duDe2NHBlkOWdFM21QWGFySE0","0B2duDe2NHBlkOWt4Z19lRWhrNEk","0B2duDe2NHBlkOXNPM21rZ2tOM00","0B2duDe2NHBlkOXU0OVUyai0yS28","0B2duDe2NHBlkOXVVQy1OTHZXRE0","0B2duDe2NHBlkOXg0c2VHaHVNTGc","0B2duDe2NHBlkOXk4Q0pER3o5WUE","0B2duDe2NHBlkQ09HQUJfY2Ziak0","0B2duDe2NHBlkQ0hIZ0ItTkxzWUk","0B2duDe2NHBlkQ0lOSlF2TDVZM0U","0B2duDe2NHBlkQ0laenc3RGVLNE0","0B2duDe2NHBlkQ1o3WGxndHdrTkE","0B2duDe2NHBlkQ2RiRUlLTE9GNEE","0B2duDe2NHBlkQ2UyOEpKdlBNcmc","0B2duDe2NHBlkQ2ZJbS1NU3Myc0E","0B2duDe2NHBlkQ3JEUDFHWFR2SjQ","0B2duDe2NHBlkQ3hBTmpWN3dPREk","0B2duDe2NHBlkQ3lYS3R5dmhKN0U","0B2duDe2NHBlkQ3phMDJFOEZzVTQ","0B2duDe2NHBlkQTJtWUZHakVZNlU","0B2duDe2NHBlkQTQ4ckZfaVdFTnc","0B2duDe2NHBlkQTRvUldWRlRjMTg","0B2duDe2NHBlkQUNwSW8tVXN0VU0","0B2duDe2NHBlkQUVteFI1ZHY1M1U","0B2duDe2NHBlkQUdhX2F6YjBpaWc","0B2duDe2NHBlkQUdldThzcElMSXM","0B2duDe2NHBlkQUlvUThvdnE5UVE","0B2duDe2NHBlkQUtQRG9qeWxRWFU","0B2duDe2NHBlkQVc1aGVFNjVpNEE","0B2duDe2NHBlkQVhGdGlPYzFrQkU","0B2duDe2NHBlkQW1EZVZEdkRlU0k","0B2duDe2NHBlkQW5Ubm9YMTZQTEE","0B2duDe2NHBlkQWJHQXE3bHh3MTQ","0B2duDe2NHBlkQWd1bGxFWlpIdW8","0B2duDe2NHBlkQXhNSW1BMzRoN2M","0B2duDe2NHBlkQi13aWZ4bXo3Z0k","0B2duDe2NHBlkQjRaSjlwSmROYWs","0B2duDe2NHBlkQjlLLVdMWGxMS3c","0B2duDe2NHBlkQkRIRDI2dmZJbjA","0B2duDe2NHBlkQklndlY0ZjFJckk","0B2duDe2NHBlkQkppT0k1amI1SzA","0B2duDe2NHBlkQktlSER2OWdpUVk","0B2duDe2NHBlkQktvWUFQOWFPU1E","0B2duDe2NHBlkQl9XeUdTTGRZTmc","0B2duDe2NHBlkQlF3Z3c3Wk9iZWs","0B2duDe2NHBlkQlMzLTcwUDFhTnc","0B2duDe2NHBlkQlNKS3hQeVcybkE","0B2duDe2NHBlkQm1LenRDeUk2Nm8","0B2duDe2NHBlkQmFndVdTc1R0UTA","0B2duDe2NHBlkQmVIdHpCS2g1TlU","0B2duDe2NHBlkQnZHTWxrT21IM0k","0B2duDe2NHBlkQng4d1A4SHh0Sm8","0B2duDe2NHBlkQzlzZmtVVXByUkk","0B2duDe2NHBlkR0JVaUszMFp6Wm8","0B2duDe2NHBlkR0V4SXJFdlpXOVU","0B2duDe2NHBlkR0VLQXlKbE0tUjg","0B2duDe2NHBlkR0dZOWpxT3pQS2s","0B2duDe2NHBlkR0g0TG9CLUtXcmc","0B2duDe2NHBlkR0g5WTU1cWxNM2M","0B2duDe2NHBlkR1hqMFZEZjdlb2M","0B2duDe2NHBlkR1pvS09faUlrd0E","0B2duDe2NHBlkR25OVW1uOU1WQTg","0B2duDe2NHBlkR2RXbFJIVmxyTDQ","0B2duDe2NHBlkR2RtRUkxeVBuUzQ","0B2duDe2NHBlkR2ltaktzX3FZOUk","0B2duDe2NHBlkR2oxYm02OFB0REE","0B2duDe2NHBlkR2oxbTNWZGVxeHM","0B2duDe2NHBlkR2ppRDBzbnViT2s","0B2duDe2NHBlkRDBXMzlnc3pXOWs","0B2duDe2NHBlkRDBZYXVRaVM2WUE","0B2duDe2NHBlkRDI5cGR2QV9oZlE","0B2duDe2NHBlkRDJORmVfQ21UZjA","0B2duDe2NHBlkRDNtUThKQ1BlTHc","0B2duDe2NHBlkRDRUcmdXanhkLVU","0B2duDe2NHBlkRE41QmRSS2hQVnc","0B2duDe2NHBlkRE9YN1B3ZDFuNFU","0B2duDe2NHBlkRElQT2toZElHVm8","0B2duDe2NHBlkRF9vNzJiNjQ1aEE","0B2duDe2NHBlkRG9yOXhsd1IyQ28","0B2duDe2NHBlkRGZvQVI3R21yd2s","0B2duDe2NHBlkRHFIWmtCQXJyc2c","0B2duDe2NHBlkRHdyOEFaeWRiSTQ","0B2duDe2NHBlkRTNuZ1V3ZUJmelU","0B2duDe2NHBlkRTZKa3lPelZEM2s","0B2duDe2NHBlkRTZZeXl6cTJEbms","0B2duDe2NHBlkRTl5RmJXcS1wcEU","0B2duDe2NHBlkRU00NkF4NjBKYW8","0B2duDe2NHBlkRU4tTkN6b1o1V3c","0B2duDe2NHBlkRURCNEIxU2E0am8","0B2duDe2NHBlkRUZUSzRFTnIxVFE","0B2duDe2NHBlkRUxMOEdITjh1cVU","0B2duDe2NHBlkRVhaWXZrZlktTG8","0B2duDe2NHBlkRWl5TXN0R2pWM2M","0B2duDe2NHBlkRXFuVFlrcGJYdXM","0B2duDe2NHBlkRXZSN0c3T0lhNDg","0B2duDe2NHBlkRjQ0b3dwYjNDLXc","0B2duDe2NHBlkRjVpM21aRUQtY0U","0B2duDe2NHBlkRk5WdENNZi1ya0k","0B2duDe2NHBlkRk9BMGNSVV9haFE","0B2duDe2NHBlkRkRpYktuQWxiMVU","0B2duDe2NHBlkRkVnbGJMdFVfZ0k","0B2duDe2NHBlkRkxVelJENWdPWW8","0B2duDe2NHBlkRl9lbi1weE01NTA","0B2duDe2NHBlkRlNRV1pXa3YtOHc","0B2duDe2NHBlkRlVPZkpWblYxdUE","0B2duDe2NHBlkRmM0SmhfRnl5SWs","0B2duDe2NHBlkRmZKSEQ3aG1nV3M","0B2duDe2NHBlkRmhrcC0tRVdrTU0","0B2duDe2NHBlkRnBNOUN4OEdpUkE","0B2duDe2NHBlkRnNVUDdVN3NUR00","0B2duDe2NHBlkRy1VMG5XeWRnalE","0B2duDe2NHBlkS056TlNqUlhlY2s","0B2duDe2NHBlkS0k1dGZySjUybUk","0B2duDe2NHBlkS185ci1sck5fVTA","0B2duDe2NHBlkS2FOSkpFU1JQblE","0B2duDe2NHBlkS2Y5NmV1d01nd1k","0B2duDe2NHBlkS2lITTVPaUFjSzg","0B2duDe2NHBlkS2xWbTlTdmQteVk","0B2duDe2NHBlkS3EtTlBDU292OFk","0B2duDe2NHBlkS3VRM0pTaTFBMGs","0B2duDe2NHBlkSDZocWZkMUg3MjA","0B2duDe2NHBlkSDZpU3QxVWdqOTQ","0B2duDe2NHBlkSDhXMnZxLTZWSnM","0B2duDe2NHBlkSEFLcVBoTmdPYXc","0B2duDe2NHBlkSENJdWo0Y1VEYnM","0B2duDe2NHBlkSEQxVDVWeWNQZ0k","0B2duDe2NHBlkSFFpeFRrcWZYOU0","0B2duDe2NHBlkSFNoVjdIMDg4SWs","0B2duDe2NHBlkSFZBTVZGUTU1T3c","0B2duDe2NHBlkSGR6c1B2bVRvdFU"
]

if __name__ == '__main__':
    try:

        main()

        sys.exit(0)
    except KeyboardInterrupt, e: # Ctrl-C
        raise e
    except SystemExit, e: # sys.exit()
        raise e
    except Exception, e:
        logging.error('ERROR, UNEXPECTED EXCEPTION')
        logging.error(str(e))
        traceback.print_exc()
        os._exit(1)