#!/bin/sh

OPT_MODE="create"

while getopts :m:s: FLAG; do
  case $FLAG in
    m)  #set option "create"
      OPT_MODE=$OPTARG
      echo "-mode used: $OPTARG"
      ;;
    s)  #set option "slug-domains"
      OPT_SLUG_DOMAINS=$OPTARG
      echo "-slug-domains used: $OPTARG"
      ;;
    \?) #unrecognized option - show help
      echo -e \\n"Option -${BOLD}$OPTARG${NORM} not allowed."
      exit 2
      ;;
  esac
done

[ -z "$OPT_SLUG_DOMAINS" ]
if [ -z "$OPT_SLUG_DOMAINS" ]; then 
  echo "slug domains mandatory"
  exit 2
fi

SLUG_DOMAIN_ARRAY=(${OPT_SLUG_DOMAINS//,/ })

#pour respecter l indentation
OLD_IFS="$IFS"
IFS=

read -d '' ingress <<EOF
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: activiti-ingress
spec:
  rules:
  - http:
      paths:
      - path: /default/activiti-explorer/*
        backend:
           serviceName: activiti-explorer-default-service
           servicePort: 8080
      - path: /default/activiti-rest/*
        backend:
           serviceName: activiti-rest-default-service
           servicePort: 8080
EOF

for i in "${SLUG_DOMAIN_ARRAY[@]}"
do
read -d '' mapping <<EOF2
      - path: /$i/activiti-rest/*
        backend:
           serviceName: activiti-rest-$i-service
           servicePort: 8080
EOF2
ingress=$ingress$mapping
done

echo $OPT_MODE" following ingress"
echo "****************************************************************"
echo $ingress
echo "****************************************************************"

if [ $OPT_MODE = "create" ]; then
  echo $ingress | kubectl create -f -
elif [ $OPT_MODE = "replace" ]; then
  echo $ingress | kubectl replace -f -
fi

#reset initial IFS
IFS="$OLD_IFS"