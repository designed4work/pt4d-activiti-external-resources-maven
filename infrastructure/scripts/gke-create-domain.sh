#!/bin/sh

OPT_MODE="create"

while getopts :m:d:s:b: FLAG; do
  case $FLAG in
    m)  #set option "create"
      OPT_MODE=$OPTARG
      echo "-mode used: $OPTARG"
      ;;
    d)  #set option "create"
      OPT_DOMAIN=$OPTARG
      echo "-domain used: $OPTARG"
      ;;
    s)  #set option "slug-domains"
      OPT_SLUG_DOMAIN=$OPTARG
      echo "-slug-domain used: $OPTARG"
      ;;
    b)  #set option "slug-domains"
      OPT_DATABASE_NAME=$OPTARG
      echo "-database name used: $OPTARG"
      ;;
    \?) #unrecognized option - show help
      echo -e \\n"Option -${BOLD}$OPTARG${NORM} not allowed."
      exit 2
      ;;
  esac
done

[ -z "$OPT_DOMAIN" ]
if [ -z "$OPT_DOMAIN" ]; then 
  echo "domain mandatory"
  exit 2
fi

[ -z "$OPT_SLUG_DOMAIN" ]
if [ -z "$OPT_SLUG_DOMAIN" ]; then 
  echo "slug domain mandatory"
  exit 2
fi

[ -z "$OPT_DATABASE_NAME" ]
if [ -z "$OPT_DATABASE_NAME" ]; then 
  echo "database mandatory"
  exit 2
fi

#pour respecter l indentation
OLD_IFS="$IFS"
IFS=

read -d '' deployment  <<EOF
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: activiti-rest-$OPT_SLUG_DOMAIN
spec:  # specification of the pod’s contents
  template:
    metadata:
      labels:
        app: activiti-rest
        domain: $OPT_DOMAIN
    spec:
      containers:
      - name: activiti-rest-container
        image: "eu.gcr.io/powertools-v2/activiti-rest"
        ports:
        - containerPort: 8080
        env:
        - name: SLUG_DOMAIN
          value: "$OPT_SLUG_DOMAIN"        
        - name: TOMCAT_ADMIN_USER
          value: "tomcatadmin"
        - name: TOMCAT_ADMIN_PASSWORD
          value: "hAgMkALeAGkhQ7UtpXTHADdH"
        - name: DB_HOST
          value: "127.0.0.1"
        - name: DB_PORT
          value: "3306"
        - name: DB_NAME
          value: "$OPT_DATABASE_NAME"
        - name: DB_PASS
          valueFrom:
            secretKeyRef:
              name: cloudsql-db-credentials
              key: password
        - name: DB_USER
          valueFrom:
            secretKeyRef:
              name: cloudsql-db-credentials
              key: username
        - name: DB_TYPE
          value: "mysql"
        - name: API_DOMAIN
          value: "devcirruseo.com"
        resources:
          requests:
            cpu: "60m"
      - name: cloudsql-proxy
        image: gcr.io/cloudsql-docker/gce-proxy:1.09
        command: ["/cloud_sql_proxy", "--dir=/cloudsql",
                  "-instances=powertools-v2:us-central1:workflow=tcp:3306",
                  "-credential_file=/secrets/cloudsql/credentials.json"]
        volumeMounts:
          - name: cloudsql-instance-credentials
            mountPath: /secrets/cloudsql
            readOnly: true
          - name: ssl-certs
            mountPath: /etc/ssl/certs
          - name: cloudsql
            mountPath: /cloudsql
        resources:
            requests:
              cpu: "10m"
      volumes:
      - name: cloudsql-instance-credentials
        secret:
          secretName: cloudsql-instance-credentials
      - name: ssl-certs
        hostPath:
          path: /etc/ssl/certs
      - name: cloudsql
        emptyDir:
---
apiVersion: v1
kind: Service
metadata:
  name: activiti-rest-$OPT_SLUG_DOMAIN-service
spec:
    selector:
        domain: $OPT_DOMAIN
        app: activiti-rest
    type: NodePort
    ports:
        - port: 8080
EOF

echo "Creating following ingress"
echo "********************************"
echo $deployment
echo "********************************"

if [ $OPT_MODE = "create" ]; then
  echo $deployment | kubectl create -f -
elif [ $OPT_MODE = "replace" ]; then
  echo $deployment | kubectl replace -f -
fi

IFS="$OLD_IFS"

