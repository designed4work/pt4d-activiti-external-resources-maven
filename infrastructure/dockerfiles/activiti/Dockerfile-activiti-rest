#
# Ubuntu 14.04 with activiti Dockerfile
#
# Pull base image.
### http://blog.docker.com/2015/03/updates-available-to-popular-repos-update-your-images/
# dockerfile/java renamed to java
### 
FROM openjdk:8
MAINTAINER Stephane Cosmeur "stephane.cosmeur@designedforwork.com"

EXPOSE 8080

ENV TOMCAT_VERSION 8.0.38
ENV ACTIVITI_VERSION 5.22.0
ENV MYSQL_CONNECTOR_JAVA_VERSION 5.1.40

ENV ACTIVITI_WAR_NAME activiti-rest

# Tomcat
RUN wget http://archive.apache.org/dist/tomcat/tomcat-8/v${TOMCAT_VERSION}/bin/apache-tomcat-${TOMCAT_VERSION}.tar.gz -O /tmp/catalina.tar.gz && \
	tar xzf /tmp/catalina.tar.gz -C /opt && \
	ln -s /opt/apache-tomcat-${TOMCAT_VERSION} /opt/tomcat && \
	rm /tmp/catalina.tar.gz && \
	rm -rf /opt/tomcat/webapps/examples && \
	rm -rf /opt/tomcat/webapps/docs && \
	rm -rf /opt/tomcat/webapps/host-manager && \
	rm -rf /opt/tomcat/webapps/manager

# To install jar files first we need to deploy war files manually
RUN wget https://github.com/Activiti/Activiti/releases/download/activiti-${ACTIVITI_VERSION}/activiti-${ACTIVITI_VERSION}.zip -O /tmp/activiti.zip && \
 	unzip /tmp/activiti.zip -d /opt/activiti && \
	unzip /opt/activiti/activiti-${ACTIVITI_VERSION}/wars/${ACTIVITI_WAR_NAME}.war -d /opt/tomcat/webapps/${ACTIVITI_WAR_NAME} && \
	rm -f /tmp/activiti.zip

# Add roles
ADD assets /assets
RUN cp /assets/config/tomcat/tomcat-users.xml /opt/apache-tomcat-${TOMCAT_VERSION}/conf/

#Change server.xml (logging conf)
RUN cp /assets/config/tomcat/server.xml /opt/apache-tomcat-${TOMCAT_VERSION}/conf/

# Copy jars from assets to tomcat lib
RUN unzip /assets/pt4d-libs/* -d /opt/tomcat/webapps/${ACTIVITI_WAR_NAME}/WEB-INF/classes/ && \
	rm -rf /assets/pt4d-libs

# Copy jars from assets to tomcat lib
RUN cp /assets/libs/* /opt/tomcat/webapps/${ACTIVITI_WAR_NAME}/WEB-INF/lib/ && \
	rm -rf /assets/libs

CMD ["sh", "-c", "/assets/init ${ACTIVITI_WAR_NAME}"]