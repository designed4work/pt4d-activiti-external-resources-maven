package com.pt4d.config;

public class Pt4dConstants {
	public final static String VARIABLE_RESULT = "pt4d_result";
	public final static String VARIABLE_INITIATOR = "pt4d_initiator";
	public final static String VARIABLE_STATUS = "pt4d_status";
	public final static String VARIABLE_FILE_ID = "pt4d_file_id";
	public final static String VARIABLE_FILE = "pt4d_file";
	public final static String VARIABLE_FORM_PROPERTIES = "pt4d_form_properties";
	public final static String VARIABLE_PT4D_ID = "pt4d_id";
	
	public final static String FORM_SUBMIT_BUTTON_ID = "btn_submit";
	
	public final static String STATUS_STARTED = "started";
	public final static String STATUS_TERMINATED = "terminated";
	public final static String STATUS_ERROR = "error";

	public final static String PT4D_DATE_FORMAT = "yyyy-MM-dd";
	
	
}
