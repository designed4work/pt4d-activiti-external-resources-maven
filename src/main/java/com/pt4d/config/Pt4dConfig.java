
package com.pt4d.config;

public class Pt4dConfig {
	public final static String API_KEY = System.getenv("API_KEY");
	public final static String API_DEFAULT_MODULE = "pt4dapi";
	public final static String API_DOMAIN = System.getenv("API_DOMAIN");
	public final static String APP_ID = System.getenv("APP_ID");
	public final static String DEFAULT_USER_ID = "workflow-java-client";
	public final static String USER_PASSWORD = "zgsxErVxVTXoR8dNYdxRXJzg";
	
	private final static String API_DEFAULT_URL = "https://" + API_DEFAULT_MODULE + "-dot-" + APP_ID + ".appspot.com/_ah/api/";
	private final static String API_DEFAULT_LOCAL_URL = "http://10.200.10.1:8088/_ah/api/";
	
	public static boolean isApiLocal() {
		if (System.getenv("API_IS_LOCAL") != null  &&  System.getenv("API_IS_LOCAL").equals("true")) {
			return true;
		}	
		return false;
	}
	
	public static String getApiUrl() {
		if (isApiLocal()) {
			return Pt4dConfig.API_DEFAULT_LOCAL_URL;
		} else {
			return Pt4dConfig.API_DEFAULT_URL;
		}
	}
}
