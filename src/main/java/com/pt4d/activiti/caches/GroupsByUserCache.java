package com.pt4d.activiti.caches;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.appspot.powertools_v2.powertools.model.ApiV2MessagesListOfGroupsMessage;
import com.pt4d.activiti.beans.Pt4dFileManager;
import com.pt4d.activiti.tools.CrunchifyInMemoryCache;

public class GroupsByUserCache extends CrunchifyInMemoryCache<String, ApiV2MessagesListOfGroupsMessage> {
	
	private static Logger log = LoggerFactory.getLogger(Pt4dFileManager.class);
	private static GroupsByUserCache instance = null;

	public GroupsByUserCache(long crunchifyTimeToLive, long crunchifyTimerInterval, int maxItems) {
		super(crunchifyTimeToLive, crunchifyTimerInterval, maxItems);
		
		log.info(String.format("GroupsByUserCache(%d, long %d, int %d)", crunchifyTimeToLive, crunchifyTimerInterval, maxItems));
	}
	
	// Lazy Initialization (If required then only)
	public static GroupsByUserCache getInstance() {
		if (instance == null) {
			// Thread Safe. Might be costly operation in some case
			synchronized (GroupsByUserCache.class) {
				if (instance == null) {
					instance = new GroupsByUserCache(120, 5, 1000);
				}
			}
		}
		return instance;
	}
}
