package com.pt4d.activiti.listeners.tasks;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.task.IdentityLink;
import org.activiti.engine.task.IdentityLinkType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.appspot.powertools_v2.powertools.Powertools;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesFileMessage;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesTagMessage;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.pt4d.activiti.base.Pt4dTaskListeners;
import com.pt4d.activiti.exceptions.Pt4dException;
import com.pt4d.activiti.tools.PowertoolsService;
import com.pt4d.config.Pt4dConfig;
import com.pt4d.config.Pt4dConstants;

public abstract class SetRoleAsIdentityLinkType extends Pt4dTaskListeners {
	
	private static Logger log = LoggerFactory.getLogger(SetRoleAsIdentityLinkType.class);
	private static Powertools service = PowertoolsService.createPowertoolsService();
	
	abstract String getDriveType();
	
	abstract String getIdentityLinkType();

	public void setAs(DelegateTask delegateTask) throws Pt4dException {		
		Set<IdentityLink> candidates = delegateTask.getCandidates();
		
		Object fileId = delegateTask.getVariable(Pt4dConstants.VARIABLE_FILE_ID);
		
		log.info("Searching for " + getDriveType() + " permissions on file " + fileId + " to add it as " + getIdentityLinkType());
		
		List<String> userIds = new ArrayList();
		List<String> groupIds = new ArrayList();
		if(fileId != null) {
			try {
				ApiV2MessagesFileMessage fileMessage = service.files().get(Pt4dConfig.API_KEY, Pt4dConfig.API_DOMAIN, Pt4dConfig.DEFAULT_USER_ID, (String)fileId)
						.setWithDriveData(true).setFetchGoogleIds(true).execute(); 
				JsonObject driveDataJson = (new JsonParser()).parse(fileMessage.getDriveData()).getAsJsonObject();
				JsonArray permissionsJson = driveDataJson.get("permissions").getAsJsonArray();
				
				for (Iterator<JsonElement> it = permissionsJson.iterator(); it.hasNext();) {
					JsonObject permissionJson = it.next().getAsJsonObject();
					if(permissionJson.get("role").getAsString().equals(getDriveType())) {
						if(permissionJson.has("googleId")) {							
							String googleId = permissionJson.get("googleId").getAsString();
							if(permissionJson.get("type").getAsString().equals("user")) {
								if ( !isAlreadyCandidate(googleId, candidates) ) {
									log.info("Adding user " + googleId + " as " + getIdentityLinkType() + " because he is " + getDriveType() + " on file " + fileId);
									delegateTask.addUserIdentityLink(googleId, getIdentityLinkType());
								}
							} else if(permissionJson.get("type").getAsString().equals("group")) {
								if ( !isAlreadyCandidate(googleId, candidates) ) {
									log.info("Adding group " + googleId + " as " + getIdentityLinkType() + " because he is " + getDriveType() + " on file " + fileId);
									delegateTask.addGroupIdentityLink(googleId, getIdentityLinkType());
								}
							} else {
								log.warn("googleId " + permissionJson.has("googleId") + " found but type " + permissionJson.get("type").getAsString() + " does not match");
							}
						} else {
							log.warn("googleId not found for permission : " + permissionJson.toString());
						}
					}
				}	
			} catch (IOException e) {
				e.printStackTrace();
				log.error("Cannot get file " + fileId);
				throw new Pt4dException("Cannot get file " + fileId);
			}
		} else {
			log.error("No variable " + Pt4dConstants.VARIABLE_FILE_ID + " found.");
			throw new Pt4dException("No variable " + Pt4dConstants.VARIABLE_FILE_ID + " found.");
		}
	}
	
	private Boolean isAlreadyCandidate(String googleId, Set<IdentityLink> candidates) {
		for (Iterator<IdentityLink> it = candidates.iterator(); it.hasNext();) {
			IdentityLink identityLink = it.next();
			if (googleId.equals(identityLink.getGroupId()) || googleId.equals(identityLink.getUserId())) {
				if ( !identityLink.getType().equals(getIdentityLinkType()) ) {
					log.warn("googleId " + googleId + " is already present but not as " + getIdentityLinkType());
				} else {
					return true;
				}
			}
		}
		return false;
	}
}
