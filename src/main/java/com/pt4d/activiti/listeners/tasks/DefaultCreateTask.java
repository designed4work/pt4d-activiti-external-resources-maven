package com.pt4d.activiti.listeners.tasks;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.activiti.engine.FormService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.form.FormProperty;
import org.activiti.engine.impl.context.Context;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.task.IdentityLink;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.appspot.powertools_v2.powertools.Powertools;
import com.appspot.powertools_v2.powertools.Powertools.Internal.EmailFromTemplate;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesEmailFromTemplateMessage;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesEmailMessage;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesEmailResponseMessage;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.pt4d.activiti.base.Pt4dTaskListeners;
import com.pt4d.activiti.beans.Pt4dFile;
import com.pt4d.activiti.exceptions.Pt4dException;
import com.pt4d.activiti.tools.PowertoolsService;
import com.pt4d.config.Pt4dConfig;
import com.pt4d.config.Pt4dConstants;

public class DefaultCreateTask extends Pt4dTaskListeners {
	
	private static Logger log = LoggerFactory.getLogger(DefaultCreateTask.class);
	private static Powertools service = PowertoolsService.createPowertoolsService();
	
	RepositoryService repositoryService = (RepositoryService) Context.getProcessEngineConfiguration().getBeans().get("repositoryService");

	public void pt4dExecute(DelegateTask delegateTask) throws Pt4dException {
		this.saveForm(delegateTask);
		this.notifyCandidates(delegateTask);
	}
	
	private void notifyCandidates(DelegateTask delegateTask) throws Pt4dException {
		Set<IdentityLink> identityLinks = delegateTask.getCandidates();
		List<String> userIds = new ArrayList();
		List<String> groupIds = new ArrayList();
		for(IdentityLink identityLink : identityLinks) {
			if(identityLink.getType() == "candidate" || identityLink.getType() == "assignee") {
				if(identityLink.getUserId() != null) {
					userIds.add(identityLink.getUserId());
				} else if(identityLink.getGroupId() != null) {
					groupIds.add(identityLink.getGroupId());
				}
			}
		}
		
		if(userIds.isEmpty() && groupIds.isEmpty()) {
			log.warn("No candidate set for this task " + delegateTask.getId() + " !");
		}
		
		ProcessDefinition processDefinition = repositoryService.getProcessDefinition(delegateTask.getProcessDefinitionId());
		
		JsonObject data = new JsonObject();
		data.addProperty("task_id", delegateTask.getId());
		data.addProperty("process_instance_id", delegateTask.getProcessInstanceId());
		data.addProperty("pt4d_id", (Long) delegateTask.getVariable(Pt4dConstants.VARIABLE_PT4D_ID));
		data.addProperty("process_instance_name", processDefinition.getName());
		data.addProperty("task_name", delegateTask.getName());
		data.addProperty("initiator", (String) delegateTask.getVariable(Pt4dConstants.VARIABLE_INITIATOR));
		if(delegateTask.getVariable(Pt4dConstants.VARIABLE_FILE) != null) {		
			Pt4dFile file = (Pt4dFile) delegateTask.getVariable(Pt4dConstants.VARIABLE_FILE);
			Gson gson = new Gson();
			data.addProperty("pt4d_file", gson.toJson(file));
		}
		
		
		if(!userIds.isEmpty()) {
			try {
				log.info("Notify Candidates");
				log.info("CALL service.internal().email().createFromTemplate()");
				ApiV2MessagesEmailFromTemplateMessage msg = new ApiV2MessagesEmailFromTemplateMessage();
				msg.setTemplateName("new_workflow_task_assigned");
				msg.setJsonData(data.toString());
				msg.setMailSubject("Task Assigned: " + delegateTask.getName());
				msg.setUserIds(userIds);
				ApiV2MessagesEmailResponseMessage resultMsg = service.internal().emailFromTemplate().create(Pt4dConfig.API_KEY, Pt4dConfig.API_DOMAIN, Pt4dConfig.DEFAULT_USER_ID, msg).execute();
				log.info("RESOPNPSE service.internal().email().createFromTemplate() => " + resultMsg.toPrettyString());
			} catch (IOException e) {
				e.printStackTrace();
				log.error(e.getMessage());
				throw new Pt4dException("Cannot create email from template for auto notification");
			}
		}
	}
	
	private void saveForm(DelegateTask delegateTask) {
		FormService formService = (FormService) Context.getProcessEngineConfiguration().getBeans().get("formService");
		
		List<FormProperty> formProperties = formService.getTaskFormData(delegateTask.getId()).getFormProperties();
		
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String sJson = gson.toJson(formProperties);
		
		ObjectMapper mapper = new ObjectMapper();
		JsonNode json = null;
		try {
			json = mapper.readTree(sJson);
		} catch (JsonProcessingException e) {
			log.error(e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			log.error(e.getMessage());
			e.printStackTrace();
		} 	
		
		log.info("Save " + Pt4dConstants.VARIABLE_FORM_PROPERTIES + ": " + json);
		delegateTask.setVariableLocal(Pt4dConstants.VARIABLE_FORM_PROPERTIES, json);
	}
}
