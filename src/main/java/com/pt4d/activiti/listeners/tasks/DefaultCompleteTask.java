package com.pt4d.activiti.listeners.tasks;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.activiti.engine.FormService;
import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.form.FormProperty;
import org.activiti.engine.impl.context.Context;
import org.activiti.engine.impl.identity.Authentication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pt4d.activiti.base.Pt4dTaskListeners;
import com.pt4d.activiti.beans.Pt4dFile;
import com.pt4d.activiti.beans.Pt4dFileManager;
import com.pt4d.config.Pt4dConstants;

public class DefaultCompleteTask extends Pt4dTaskListeners {
	
	private static Logger log = LoggerFactory.getLogger(DefaultCompleteTask.class);

	public void pt4dExecute(DelegateTask delegateTask) throws IOException {
		delegateTask.setAssignee(Authentication.getAuthenticatedUserId());
		this.saveForm(delegateTask);
		
		String fileId = (String) delegateTask.getExecution().getVariable(Pt4dConstants.VARIABLE_FILE_ID);
		if (fileId != null) {
			this.updateFile(delegateTask, fileId);
		}
	}
		
	private void saveForm(DelegateTask delegateTask) {
		FormService formService = (FormService) Context.getProcessEngineConfiguration().getBeans().get("formService");
		
		List<FormProperty> formProperties = formService.getTaskFormData(delegateTask.getId()).getFormProperties();
		
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String sJson = gson.toJson(formProperties);
		
		ObjectMapper mapper = new ObjectMapper();
		JsonNode json = null;
		try {
			json = mapper.readTree(sJson);
		} catch (JsonProcessingException e) {
			log.error(e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}
		
		//Add type name to type node in order to display form property in front end
		for (Iterator<JsonNode> iter = json.iterator(); iter.hasNext(); ) {
		    JsonNode formProperyNode = iter.next();
		    for(FormProperty formProperty : formProperties) {
		    	if (formProperty.getId().equals(formProperyNode.get("id").textValue())) {
		    		ObjectNode typeObjectNode = (ObjectNode) formProperyNode.get("type");
		    		typeObjectNode.put("name", formProperty.getType().getName());
		    	}
		    }
		}
		
		log.info("Save " + Pt4dConstants.VARIABLE_FORM_PROPERTIES + ": " + json);
		delegateTask.setVariableLocal(Pt4dConstants.VARIABLE_FORM_PROPERTIES, json);
		
		//Set pt4d_result locally
		for (Iterator<FormProperty> it = formProperties.iterator(); it.hasNext();) {
			FormProperty formProperty = it.next();
			if (formProperty.getId().equals(Pt4dConstants.FORM_SUBMIT_BUTTON_ID)) {
				log.info("Save " + Pt4dConstants.VARIABLE_RESULT + ": " + formProperty.getValue());
				delegateTask.setVariableLocal(Pt4dConstants.VARIABLE_RESULT, formProperty.getValue());
			}
		}
	}

	private void updateFile(DelegateTask delegateTask, String fileId) throws IOException {
		log.info(String.format("Refreshing file %s", fileId));
		Pt4dFile file = Pt4dFileManager.getFileById(fileId);
		log.info("Save " + Pt4dConstants.VARIABLE_FILE + ": " + file);
		delegateTask.getExecution().setVariable(Pt4dConstants.VARIABLE_FILE, file);
	}
}
