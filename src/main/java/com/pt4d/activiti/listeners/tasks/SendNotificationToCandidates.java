package com.pt4d.activiti.listeners.tasks;

import org.activiti.engine.delegate.DelegateTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.appspot.powertools_v2.powertools.Powertools;
import com.pt4d.activiti.base.Pt4dTaskListeners;
import com.pt4d.activiti.tools.PowertoolsService;

public class SendNotificationToCandidates extends Pt4dTaskListeners {
	
	private static Logger log = LoggerFactory.getLogger(SendNotificationToCandidates.class);
	private static Powertools service = PowertoolsService.createPowertoolsService();

	public void pt4dExecute(DelegateTask delegateTask) {
		service.internal().emailFromTemplate();
	}

}
