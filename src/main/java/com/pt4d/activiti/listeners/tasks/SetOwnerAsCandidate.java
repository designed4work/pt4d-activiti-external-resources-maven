package com.pt4d.activiti.listeners.tasks;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.task.IdentityLink;
import org.activiti.engine.task.IdentityLinkType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.appspot.powertools_v2.powertools.Powertools;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesFileMessage;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesTagMessage;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.pt4d.activiti.base.Pt4dTaskListeners;
import com.pt4d.activiti.exceptions.Pt4dException;
import com.pt4d.activiti.tools.PowertoolsService;
import com.pt4d.config.Pt4dConfig;
import com.pt4d.config.Pt4dConstants;

public class SetOwnerAsCandidate extends SetRoleAsIdentityLinkType {
	
	private static Logger log = LoggerFactory.getLogger(SetOwnerAsCandidate.class);
	
	@Override
	String getDriveType() {
		return "owner";
	}
	@Override
	String getIdentityLinkType() {
		return IdentityLinkType.CANDIDATE;
	}
	@Override
	public void pt4dExecute(DelegateTask delegateTask) throws Pt4dException {
		setAs(delegateTask);
	}

}
