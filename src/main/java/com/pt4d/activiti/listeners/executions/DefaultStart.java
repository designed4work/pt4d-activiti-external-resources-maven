package com.pt4d.activiti.listeners.executions;

import org.activiti.engine.FormService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.activiti.engine.impl.context.Context;
import org.activiti.engine.repository.ProcessDefinition;
import org.omg.CORBA.INTERNAL;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.appspot.powertools_v2.powertools.Powertools;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesCounterMessage;
import com.google.api.client.util.LoggingByteArrayOutputStream;
import com.pt4d.activiti.beans.Pt4dFile;
import com.pt4d.activiti.beans.Pt4dFileManager;
import com.pt4d.activiti.tools.PowertoolsService;
import com.pt4d.config.Pt4dConfig;
import com.pt4d.config.Pt4dConstants;

public class DefaultStart implements ExecutionListener {
	
	private static Logger log = LoggerFactory.getLogger(DefaultStart.class);
	private static Powertools service = PowertoolsService.createPowertoolsService();
	
	RuntimeService runtimeService = (RuntimeService) Context.getProcessEngineConfiguration().getBeans().get("runtimeService");
	RepositoryService repositoryService = (RepositoryService) Context.getProcessEngineConfiguration().getBeans().get("repositoryService");

	@Override
	public void notify(DelegateExecution execution) throws Exception {
		String fileId = (String) execution.getVariable(Pt4dConstants.VARIABLE_FILE_ID);
		String initiator = (String) execution.getVariable(Pt4dConstants.VARIABLE_INITIATOR);
		
		log.info("Starting workflow on file ID : " + fileId + ", started by: " + initiator);
		
		//Setting file variabLe
		if (fileId != null) {
			Pt4dFile file = Pt4dFileManager.getFileById(fileId);
			log.info("Save " + Pt4dConstants.VARIABLE_FILE + ": " + file);
			execution.setVariable(Pt4dConstants.VARIABLE_FILE, file);
		}
		
		//Setting status variable
		log.info("Save " + Pt4dConstants.VARIABLE_STATUS + ": " + Pt4dConstants.STATUS_STARTED);
		execution.setVariable(Pt4dConstants.VARIABLE_STATUS, Pt4dConstants.STATUS_STARTED);
		
		//Setting process Name
		ProcessDefinition processDefinition = repositoryService.getProcessDefinition(execution.getProcessDefinitionId());

		log.info("Setting process name as : " + processDefinition.getName());
		runtimeService.setProcessInstanceName(execution.getProcessInstanceId(), processDefinition.getName());
		
		//Getting pt4d id
		String mode = "";
		if (execution.getTenantId().startsWith("TEST")) {
			mode = "TEST";
		} else if (execution.getTenantId().startsWith("LIVE")) {
			mode = "LIVE";
		}
		ApiV2MessagesCounterMessage msg = service.internal().counter().increment(Pt4dConfig.API_KEY, Pt4dConfig.API_DOMAIN, Pt4dConfig.DEFAULT_USER_ID, mode + "_" + processDefinition.getKey()).execute();
		
		log.info("Save " + Pt4dConstants.VARIABLE_PT4D_ID + ": " + msg.getValue().toString());
		execution.setVariable(Pt4dConstants.VARIABLE_PT4D_ID, msg.getValue());
	}

}
