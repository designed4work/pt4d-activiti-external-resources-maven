package com.pt4d.activiti.listeners.executions;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.activiti.engine.impl.pvm.delegate.ActivityBehavior;
import org.activiti.engine.impl.pvm.delegate.ActivityExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pt4d.config.Pt4dConstants;

public class DefaultEnd implements ExecutionListener {
	
	private static Logger log = LoggerFactory.getLogger(DefaultEnd.class);

	@Override
	public void notify(DelegateExecution execution) throws Exception {
		log.info("Save " + Pt4dConstants.VARIABLE_STATUS + ": " + Pt4dConstants.STATUS_TERMINATED);
		execution.setVariable(Pt4dConstants.VARIABLE_STATUS, Pt4dConstants.STATUS_TERMINATED);
	}
}
