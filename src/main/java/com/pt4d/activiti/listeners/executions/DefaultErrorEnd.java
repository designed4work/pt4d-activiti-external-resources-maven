package com.pt4d.activiti.listeners.executions;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pt4d.config.Pt4dConstants;

public class DefaultErrorEnd implements ExecutionListener {
	
	private static Logger log = LoggerFactory.getLogger(DefaultErrorEnd.class);

	@Override
	public void notify(DelegateExecution execution) throws Exception {
		log.info("Save " + Pt4dConstants.VARIABLE_STATUS + ": " + Pt4dConstants.STATUS_ERROR);
		execution.setVariable(Pt4dConstants.VARIABLE_STATUS, Pt4dConstants.STATUS_ERROR);
	}


}
