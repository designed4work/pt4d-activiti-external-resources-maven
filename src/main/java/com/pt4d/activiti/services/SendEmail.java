package com.pt4d.activiti.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.activiti.engine.RuntimeService;
import org.activiti.engine.delegate.DelegateExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.appspot.powertools_v2.powertools.Powertools;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesEmailMessage;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesEmailResponseMessage;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesFileMessage;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesListOfTagsMessage;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesUpdateTagsMessage;
import com.google.common.base.Strings;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.pt4d.activiti.base.Pt4dJavaDelegate;
import com.pt4d.activiti.exceptions.Pt4dException;
import com.pt4d.activiti.tools.PowertoolsService;
import com.pt4d.config.Pt4dConfig;

public class SendEmail extends Pt4dJavaDelegate {

	private static Logger log = LoggerFactory.getLogger(SendEmail.class);
	private static Powertools service = PowertoolsService.createPowertoolsService();

	@Override
	public void pt4dExecute(DelegateExecution execution) throws Pt4dException {
		JsonElement userIdsParam = parameters.get("userIds");
		JsonElement emailParam = parameters.get("email");
		JsonElement emailsParam = parameters.get("emails");
		JsonElement subjectParam = parameters.get("subject");
		JsonElement contentParam = parameters.get("content");
		
		if (subjectParam == null) {
			new Pt4dException("A subject is required");
		}
		
		if (contentParam == null) {
			new Pt4dException("A content is required");
		}
		
		String subject = subjectParam.getAsString();
		String content = contentParam.getAsString();
		
		List<String> userIds = new ArrayList();
		if(userIdsParam != null) {
			JsonArray list = userIdsParam.getAsJsonArray();
			for (Iterator<JsonElement> it = list.iterator(); it.hasNext();) {
				String userId = it.next().getAsString();
				if (!userIds.contains(userId) && !Strings.isNullOrEmpty(userId)) {
					userIds.add(userId);
				}
			}
		}
		
		List<String> emails = new ArrayList();
		if(emailParam != null && !Strings.isNullOrEmpty(emailParam.getAsString())) {
			emails.add(emailParam.getAsString());
		}
		if(emailsParam != null) {
			JsonArray list = emailsParam.getAsJsonArray();
			for (Iterator<JsonElement> it = list.iterator(); it.hasNext();) {
				String email = it.next().getAsString();
				if (!emails.contains(email)  && !Strings.isNullOrEmpty(email)) {
					emails.add(email);
				}
			}
		}
		
		try {
			log.info("CALL service.internal().email().create() + subject: " + subject);
			ApiV2MessagesEmailMessage msg = new ApiV2MessagesEmailMessage();
			msg.setMailContent(content);
			msg.setMailSubject(subject);
			msg.setUserIds(userIds);
			msg.setEmails(emails);
			ApiV2MessagesEmailResponseMessage resultMsg = service.internal().email().create(Pt4dConfig.API_KEY, Pt4dConfig.API_DOMAIN, Pt4dConfig.DEFAULT_USER_ID, msg).execute();
			log.info("RESOPNPSE service.internal().email().create() => " + resultMsg.toPrettyString());
		} catch (IOException e) {
			e.printStackTrace();
			log.error(e.getMessage());
			throw new Pt4dException("Cannot create email");
		}
	}
}