package com.pt4d.activiti.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.activiti.engine.RuntimeService;
import org.activiti.engine.delegate.DelegateExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.appspot.powertools_v2.powertools.Powertools;
import com.appspot.powertools_v2.powertools.PowertoolsRequest;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesFileMessage;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesListOfTagsMessage;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesSimpleMessage;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesUpdateTagsMessage;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.pt4d.activiti.base.Pt4dJavaDelegate;
import com.pt4d.activiti.exceptions.Pt4dException;
import com.pt4d.activiti.tools.PowertoolsService;
import com.pt4d.config.Pt4dConfig;

public class MoveFile extends Pt4dJavaDelegate {

	private static Logger log = LoggerFactory.getLogger(MoveFile.class);
	private static Powertools service = PowertoolsService.createPowertoolsService();

	@Override
	public void pt4dExecute(DelegateExecution execution) throws Pt4dException {
		JsonElement fileIdParam = parameters.get("fileId");
		JsonElement targetFolderIdParam = parameters.get("targetFolderId");
		
		if (fileIdParam == null) {
			throw new Pt4dException("A file ID is required");
		}
		
		if (targetFolderIdParam == null) {
			throw new Pt4dException("A target folder id is required to move file");
		}
		
		String fileId = parameters.get("fileId").getAsString();
		String targetFolderId = parameters.get("targetFolderId").getAsString();
		
		try {
			log.info("CALL service.internal().moveFile()");
			Void resultMsg = service.internal().moveFile(Pt4dConfig.API_KEY, Pt4dConfig.API_DOMAIN, Pt4dConfig.DEFAULT_USER_ID, fileId, targetFolderId).execute();
			log.info("CALL service.internal().moveFile()");
		} catch (IOException e) {
			e.printStackTrace();
			log.error(e.getMessage());
			throw new Pt4dException("Cannot move file");
		}
	}
}