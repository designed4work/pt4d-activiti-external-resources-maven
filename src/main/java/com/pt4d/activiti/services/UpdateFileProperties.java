package com.pt4d.activiti.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.activiti.engine.RuntimeService;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.DelegateTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.appspot.powertools_v2.powertools.Powertools;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesFileAttributeMessage;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesFileMessage;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesSimpleMessage;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesAttributeSetterMessage;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesAttributesSettersMessage;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.pt4d.activiti.base.Pt4dJavaDelegate;
import com.pt4d.activiti.beans.Pt4dFile;
import com.pt4d.activiti.beans.Pt4dFileManager;
import com.pt4d.activiti.exceptions.Pt4dException;
import com.pt4d.activiti.tools.PowertoolsService;
import com.pt4d.config.Pt4dConfig;
import com.pt4d.config.Pt4dConstants;

public class UpdateFileProperties extends Pt4dJavaDelegate {

	private static Logger log = LoggerFactory.getLogger(UpdateFileProperties.class);
	private static Powertools service = PowertoolsService.createPowertoolsService();

	@Override
	public void pt4dExecute(DelegateExecution execution) throws Pt4dException {
		String fileId = parameters.get("fileId").getAsString();
		JsonArray properties = parameters.get("properties").getAsJsonArray();
		
		if (fileId.isEmpty()) {
			throw new Pt4dException("A file ID is required");
		}
		
		ApiV2MessagesAttributesSettersMessage msg = new ApiV2MessagesAttributesSettersMessage();
		ArrayList<ApiV2MessagesAttributeSetterMessage> setters = new ArrayList<ApiV2MessagesAttributeSetterMessage>();
		
		for (Iterator<JsonElement> it = properties.iterator(); it.hasNext();) {
			JsonObject property = it.next().getAsJsonObject();
			Long fileAttributeId = property.get("id").getAsLong();
			String value = property.get("value").getAsString();
			
			ApiV2MessagesAttributeSetterMessage singleSetter = new ApiV2MessagesAttributeSetterMessage();
			singleSetter.setAttributeId(fileAttributeId.toString());
			singleSetter.setValue(value);
			setters.add(singleSetter);
		}
		
		msg.setAttributesSetters(setters);
		
		try {
			log.info("CALL service.files().setType()");
			log.info(msg.toPrettyString());
			ApiV2MessagesFileMessage updatedFile = service.files().setType(Pt4dConfig.API_KEY, Pt4dConfig.API_DOMAIN, Pt4dConfig.DEFAULT_USER_ID, (long)-1, fileId, msg).execute();
			log.info("RESPONSE service.files().setType() => " + updatedFile.toPrettyString());
			Pt4dFile pt4dFile = Pt4dFileManager.getFileById(fileId);
			log.info("Save " + Pt4dConstants.VARIABLE_FILE + ": " + pt4dFile);
			execution.setVariable(Pt4dConstants.VARIABLE_FILE, pt4dFile);
		} catch (IOException e) {
			e.printStackTrace();
			log.error(e.getMessage());
			throw new Pt4dException("Cannot update attribute");
		}
	}
}