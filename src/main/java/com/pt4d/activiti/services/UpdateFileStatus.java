package com.pt4d.activiti.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.activiti.engine.RuntimeService;
import org.activiti.engine.delegate.DelegateExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.appspot.powertools_v2.powertools.Powertools;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesListOfTagsMessage;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesUpdateTagsMessage;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.pt4d.activiti.base.Pt4dJavaDelegate;
import com.pt4d.activiti.beans.Pt4dFile;
import com.pt4d.activiti.beans.Pt4dFileManager;
import com.pt4d.activiti.exceptions.Pt4dException;
import com.pt4d.activiti.tools.PowertoolsService;
import com.pt4d.config.Pt4dConfig;
import com.pt4d.config.Pt4dConstants;

public class UpdateFileStatus extends Pt4dJavaDelegate {

	private static Logger log = LoggerFactory.getLogger(UpdateFileStatus.class);
	private static Powertools service = PowertoolsService.createPowertoolsService();

	@Override
	public void pt4dExecute(DelegateExecution execution) throws Pt4dException {
		String fileId = parameters.get("fileId").getAsString();
		JsonElement statusParam = parameters.get("status");
		
		if (fileId.isEmpty()) {
			throw new Pt4dException("A file ID is required");
		}
		
		if (statusParam == null) {
			throw new Pt4dException("A file status is required");
		}
		
		String status = statusParam.getAsString();
		if (!status.equals("status-workflow-ok") && !status.equals("status-workflow-ko") && !status.equals("status-workflow-pending")) {
			throw new Pt4dException("file status can only be: status-workflow-ok, status-workflow-ko, status-workflow-pending");
		}
		
		try {
			log.info("CALL service.internal().setFileStatus()");
			log.info(status);
			service.internal()
				.setFileStatus(Pt4dConfig.API_KEY, Pt4dConfig.API_DOMAIN, Pt4dConfig.DEFAULT_USER_ID, fileId, status)
				.execute();
			log.info("RESOPNPSE service.internal().setFileStatus()");
		} catch (IOException e) {
			e.printStackTrace();
			log.error(e.getMessage());
			throw new Pt4dException("Cannot set file status");
		}
		
		log.info(String.format("Refreshing file %s", fileId));
		Pt4dFile file;
		try {
			file = Pt4dFileManager.getFileById(fileId);
		} catch (IOException e) {
			e.printStackTrace();
			log.error(e.getMessage());
			throw new Pt4dException("Cannot get file file");
		}
		log.info("Save " + Pt4dConstants.VARIABLE_FILE + ": " + file);
		execution.setVariable(Pt4dConstants.VARIABLE_FILE, file);
	}
}