package com.pt4d.activiti.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.activiti.engine.RuntimeService;
import org.activiti.engine.delegate.DelegateExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.appspot.powertools_v2.powertools.Powertools;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesCreateFileMessage;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesFileMessage;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesListOfTagsMessage;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesSimpleMessage;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesUpdateTagsMessage;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.pt4d.activiti.base.Pt4dJavaDelegate;
import com.pt4d.activiti.exceptions.Pt4dException;
import com.pt4d.activiti.tools.PowertoolsService;
import com.pt4d.config.Pt4dConfig;

public class CreateFile extends Pt4dJavaDelegate {

	private static Logger log = LoggerFactory.getLogger(CreateFile.class);
	private static Powertools service = PowertoolsService.createPowertoolsService();

	@Override
	public void pt4dExecute(DelegateExecution execution) throws Pt4dException {
		JsonElement nameParam = parameters.get("name");
		JsonElement targetFolderIdParam = parameters.get("targetFolderId");
		JsonElement templateFileIdParam = parameters.get("templateFileId");
		JsonElement outputVariableParam = parameters.get("outputVariable");
		
		if (nameParam == null) {
			throw new Pt4dException("A file name is required");
		}
		
		if (targetFolderIdParam == null) {
			throw new Pt4dException("A target folder id is required to move file");
		}
		
		if (templateFileIdParam == null) {
			throw new Pt4dException("A target folder id is required to move file");
		}
		
		String name = nameParam.getAsString();
		String targetFolderId = targetFolderIdParam.getAsString();
		String templateFileId = templateFileIdParam.getAsString();
		
		try {
			log.info("CALL service.internal().files().createFile()");
			ApiV2MessagesCreateFileMessage msg = new ApiV2MessagesCreateFileMessage();
			msg.setName(name);
			msg.setTargetFolderId(targetFolderId);
			msg.setTemplateFileId(templateFileId);
			ApiV2MessagesSimpleMessage resultMsg = service.internal().createFile(Pt4dConfig.API_KEY, Pt4dConfig.API_DOMAIN, Pt4dConfig.DEFAULT_USER_ID, msg).execute();
			log.info("CALL service.internal().files().createFile() => " + resultMsg.toPrettyString());
			
			if(outputVariableParam != null) {
				String outputVariable = outputVariableParam.getAsString();
				log.info(String.format("Setting variable %s with value %s", outputVariable, resultMsg.getContent()));
				execution.setVariable(outputVariable, resultMsg.getContent());
			}
		} catch (IOException e) {
			e.printStackTrace();
			log.error(e.getMessage());
			throw new Pt4dException("Cannot create file");
		}
	}
}