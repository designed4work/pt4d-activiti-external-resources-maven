package com.pt4d.activiti.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.activiti.engine.RuntimeService;
import org.activiti.engine.delegate.DelegateExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.appspot.powertools_v2.powertools.Powertools;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesListOfTagsMessage;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesUpdateTagsMessage;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.pt4d.activiti.base.Pt4dJavaDelegate;
import com.pt4d.activiti.beans.Pt4dFile;
import com.pt4d.activiti.beans.Pt4dFileManager;
import com.pt4d.activiti.exceptions.Pt4dException;
import com.pt4d.activiti.tools.PowertoolsService;
import com.pt4d.config.Pt4dConfig;
import com.pt4d.config.Pt4dConstants;

public class UpdateFileTags extends Pt4dJavaDelegate {

	private static Logger log = LoggerFactory.getLogger(UpdateFileTags.class);
	private static Powertools service = PowertoolsService.createPowertoolsService();

	@Override
	public void pt4dExecute(DelegateExecution execution) throws Pt4dException {
		String fileId = parameters.get("fileId").getAsString();
		JsonElement replaceAll = parameters.get("replaceAll");
		JsonElement toBeAdded = parameters.get("toBeAdded");
		JsonElement toBeDeleted = parameters.get("toBeDeleted");
		
		if (fileId.isEmpty()) {
			throw new Pt4dException("A file ID is required");
		}
		
		ApiV2MessagesUpdateTagsMessage msg = new ApiV2MessagesUpdateTagsMessage();
		if(replaceAll != null) {
			Boolean value = replaceAll.getAsBoolean();
			msg.setReplaceAll(value);
		} else {
			msg.setReplaceAll(false);
		}
		
		List<String> toBeAddedMsg = new ArrayList();
		if(toBeAdded != null) {
			JsonArray list = toBeAdded.getAsJsonArray();
			for (Iterator<JsonElement> it = list.iterator(); it.hasNext();) {
				JsonObject tagObject = it.next().getAsJsonObject();
				toBeAddedMsg.add(tagObject.get("value").getAsString());
			}
		}
		msg.setToBeAdded(toBeAddedMsg);
		
		List<String> toBeDeletedMsg = new ArrayList();
		if(toBeDeleted != null) {
			JsonArray list = toBeDeleted.getAsJsonArray();
			for (Iterator<JsonElement> it = list.iterator(); it.hasNext();) {
				JsonObject tagObject = it.next().getAsJsonObject();
				toBeDeletedMsg.add(tagObject.get("value").getAsString());
			}
		}
		msg.setToBeDeleted(toBeDeletedMsg);
			
		try {
			log.info("CALL service.files().tags().update()");
			log.info(msg.toPrettyString());
			ApiV2MessagesListOfTagsMessage resultMsg = service.files()
				.tags().update(Pt4dConfig.API_KEY, Pt4dConfig.API_DOMAIN, Pt4dConfig.DEFAULT_USER_ID, fileId, msg)
				.execute();
			log.info("RESOPNPSE service.files().tags().update() => " + resultMsg.toPrettyString());
		} catch (IOException e) {
			e.printStackTrace();
			log.error(e.getMessage());
			throw new Pt4dException("Cannot update tags");
		}
		
		log.info(String.format("Refreshing file %s", fileId));
		Pt4dFile file;
		try {
			file = Pt4dFileManager.getFileById(fileId);
		} catch (IOException e) {
			e.printStackTrace();
			log.error(e.getMessage());
			throw new Pt4dException("Cannot get file file");
		}
		log.info("Save " + Pt4dConstants.VARIABLE_FILE + ": " + file);
		execution.setVariable(Pt4dConstants.VARIABLE_FILE, file);
	}
}