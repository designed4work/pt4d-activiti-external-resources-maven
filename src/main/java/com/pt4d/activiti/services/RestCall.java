package com.pt4d.activiti.services;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Iterator;

import org.activiti.engine.delegate.DelegateExecution;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.appspot.powertools_v2.powertools.Powertools;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.pt4d.activiti.base.Pt4dJavaDelegate;
import com.pt4d.activiti.exceptions.Pt4dException;
import com.pt4d.activiti.tools.PowertoolsService;

public class RestCall extends Pt4dJavaDelegate {

	private static Logger log = LoggerFactory.getLogger(RestCall.class);
	private static Powertools service = PowertoolsService.createPowertoolsService();

	@Override
	public void pt4dExecute(DelegateExecution execution) throws Pt4dException {
		JsonElement methodParam = parameters.get("method");
		JsonElement protocolParam = parameters.get("protocol");
		JsonElement hostParam = parameters.get("host");
		JsonElement urlParam = parameters.get("url");
		JsonElement bodyParam = parameters.get("body");
		JsonElement headersParam = parameters.get("headers");
		JsonElement responseVariableParam = parameters.get("responseVariable");
		JsonElement responseCodeVariableParam = parameters.get("responseCodeVariable");
		
		if (methodParam == null) {
			throw new Pt4dException("A method is required");
		}
		if (protocolParam == null) {
			throw new Pt4dException("A protocol is required");
		}
		if (hostParam == null) {
			throw new Pt4dException("A host is required");
		}
		
		String method = methodParam.getAsString();
		String protocol = protocolParam.getAsString();
		String host = hostParam.getAsString();
		
		//Building URL
		String url = "";
		if (urlParam != null) {
			url = urlParam.getAsString();
			url = url.startsWith("/") ? url.substring(1) : url;
		} 		
		String fullUrl = String.format("%s://%s/%s", protocol, host, url);
		
		//Preparing body if specified
		HttpEntity httpEntity = null;
		if (bodyParam != null && !bodyParam.getAsString().isEmpty()) {
			try {
				httpEntity = new StringEntity(bodyParam.getAsString());
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
				throw new Pt4dException("Cannot set http body");
			}
		}
		
		//Builind proper object according to http method
		HttpRequestBase request;
		switch (method) {
			case "GET": request = new HttpGet(fullUrl); break;
			case "POST": request = new HttpPost(fullUrl); ((HttpPost)request).setEntity(httpEntity); break;
			case "PUT": request = new HttpPut(fullUrl); ((HttpPut)request).setEntity(httpEntity); break;
			case "DELETE": request = new HttpDelete(fullUrl); break;
			default: throw new Pt4dException(String.format("Unkown method: %s", method));
		}
		
		//Adding Authorization header if specified
		if (headersParam != null) {
			JsonArray list = headersParam.getAsJsonArray();
			for (Iterator<JsonElement> it = list.iterator(); it.hasNext();) {
				JsonObject jsonHeader = it.next().getAsJsonObject();
				String key = jsonHeader.get("key").getAsString();
				String value = jsonHeader.get("value").getAsString();
				
				log.info(String.format("Adding header %s: %s", key, value));
				request.addHeader(key, value);
			}
		}
		
		//Perform request
		log.info(String.format("Performing request: %s %s", method, fullUrl));
		try {
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpResponse response = httpClient.execute(request);
			
			log.info(String.format("Response code: %s", Integer.toString(response.getStatusLine().getStatusCode())));
			
			//Setting response code
			if(responseCodeVariableParam != null && !responseCodeVariableParam.getAsString().isEmpty()) {
				String responseCodeVariable = responseCodeVariableParam.getAsString();
				log.info(String.format("Setting variable %s with value %d", responseCodeVariable, response.getStatusLine().getStatusCode()));
				execution.setVariable(responseCodeVariable, response.getStatusLine().getStatusCode());
			}
			
			//Setting Response content, try to parse JSON, if not set as string
			if(responseVariableParam != null && !responseVariableParam.getAsString().isEmpty()) {
				String responseVariable = responseVariableParam.getAsString();
				String stringResponse = null;
				JsonNode jsonResponse = null;
				if (response.getEntity() != null) {
					String encoding = response.getEntity().getContentEncoding() == null ? "UTF-8" : response.getEntity().getContentEncoding().getValue();
					log.info(String.format("Read response with encoding: %s", encoding));
					
					stringResponse = IOUtils.toString(response.getEntity().getContent(), encoding); 
					ObjectMapper mapper = new ObjectMapper();
					try {
						jsonResponse = mapper.readTree(stringResponse);
					} catch (Exception e) {
						log.info(String.format("Cannot parse response in json %s", e.getMessage()));
						e.printStackTrace();
					}
				}
				if (jsonResponse != null) {
					log.info(String.format("Setting variable %s as JSON", responseVariable));
					execution.setVariable(responseVariable, jsonResponse);
				} else if (stringResponse != null){
					log.info(String.format("Setting variable %s as String", responseVariable));
					execution.setVariable(responseVariable, stringResponse);					
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
			log.error(e.getMessage());
			throw new Pt4dException(String.format("Cannot perform API call: %s", e.getMessage()));
		}
	}
}