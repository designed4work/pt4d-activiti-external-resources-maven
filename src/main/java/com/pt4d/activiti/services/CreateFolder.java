package com.pt4d.activiti.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.activiti.engine.RuntimeService;
import org.activiti.engine.delegate.DelegateExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.appspot.powertools_v2.powertools.Powertools;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesCreateFileMessage;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesCreateFolderMessage;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesFileMessage;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesListOfTagsMessage;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesSimpleMessage;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesUpdateTagsMessage;
import com.google.api.client.repackaged.com.google.common.base.Strings;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.pt4d.activiti.base.Pt4dJavaDelegate;
import com.pt4d.activiti.exceptions.Pt4dException;
import com.pt4d.activiti.tools.PowertoolsService;
import com.pt4d.config.Pt4dConfig;

public class CreateFolder extends Pt4dJavaDelegate {

	private static Logger log = LoggerFactory.getLogger(CreateFolder.class);
	private static Powertools service = PowertoolsService.createPowertoolsService();

	@Override
	public void pt4dExecute(DelegateExecution execution) throws Pt4dException {
		JsonElement nameParam = parameters.get("name");
		JsonElement targetFolderIdParam = parameters.get("targetFolderId");
		JsonElement outputVariableParam = parameters.get("outputVariable");
		
		if (nameParam == null) {
			throw new Pt4dException("A file name is required");
		}
		
		if (targetFolderIdParam == null) {
			throw new Pt4dException("A target folder id is required to move file");
		}
		
		String name = nameParam.getAsString();
		String targetFolderId = targetFolderIdParam.getAsString();
		
		try {
			log.info("CALL service.internal().internal().createFolder()");
			ApiV2MessagesCreateFolderMessage msg = new ApiV2MessagesCreateFolderMessage();
			msg.setName(name);
			msg.setTargetFolderId(targetFolderId);
			ApiV2MessagesSimpleMessage resultMsg = service.internal().createFolder(Pt4dConfig.API_KEY, Pt4dConfig.API_DOMAIN, Pt4dConfig.DEFAULT_USER_ID, msg).execute();
			log.info("CALL service.internal().internal().createFolder() => " + resultMsg.toPrettyString());
			
			if(outputVariableParam != null) {
				String outputVariable = outputVariableParam.getAsString();
				log.info(String.format("Setting variable %s with value %s", outputVariable, resultMsg.getContent()));
				execution.setVariable(outputVariable, resultMsg.getContent());
			}
		} catch (IOException e) {
			e.printStackTrace();
			log.error(e.getMessage());
			throw new Pt4dException("Cannot create folder");
		}
	}
}