package com.pt4d.activiti.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.activiti.engine.delegate.DelegateExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.appspot.powertools_v2.powertools.Powertools;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesGroupMessage;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesSetPermissionsRequestMessage;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesSetPermissionsResponseMessage;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.client.util.Strings;
import com.google.gson.JsonElement;
import com.pt4d.activiti.base.Pt4dJavaDelegate;
import com.pt4d.activiti.exceptions.Pt4dException;
import com.pt4d.activiti.tools.PowertoolsService;
import com.pt4d.config.Pt4dConfig;

public class SetPermissions extends Pt4dJavaDelegate {

	private static Logger log = LoggerFactory.getLogger(SetPermissions.class);
	private static Powertools service = PowertoolsService.createPowertoolsService();

	@Override
	public void pt4dExecute(DelegateExecution execution) throws Pt4dException {
		JsonElement fileIdParam = parameters.get("fileId");
		JsonElement actionParam = parameters.get("action");
		JsonElement idsParam = parameters.get("ids");
		JsonElement groupIdsParam = parameters.get("groupIds");		
		JsonElement replaceExistingPermissionsParam = parameters.get("replaceExistingPermissions");
		JsonElement individualActionRoleParam = parameters.get("individualActionRole");
		JsonElement outputVariableParam = parameters.get("outputVariable");
		
		if (fileIdParam == null) {
			throw new Pt4dException("A file ID is required");
		}
		if (actionParam == null) {
			throw new Pt4dException("An action is required");
		}
		
		String fileId = fileIdParam.getAsString();
		String action = actionParam.getAsString();
		
		List<String> ids = new ArrayList<String>();
		if (idsParam != null) {
			String stringIds = idsParam.getAsString();
			if(!Strings.isNullOrEmpty(stringIds)) {
				ids = new ArrayList<String>(Arrays.asList(stringIds.split(",")));
			}
		}
		
		if (groupIdsParam != null) {
			String stringIds = groupIdsParam.getAsString();
			if(!Strings.isNullOrEmpty(stringIds)) {
				List<String> groupIds = Arrays.asList(stringIds.split(","));
				for (String groupId : groupIds) {
					if(!ids.contains(groupId)) {
						ids.add(groupId);
					}
				}
			}
		}
		
		Boolean replaceExistingPermissions = false;
		if(replaceExistingPermissionsParam != null ) {
			replaceExistingPermissions = replaceExistingPermissionsParam.getAsBoolean();
		}
		
		String individualActionRole = null;
		if(individualActionRoleParam != null) {
			individualActionRole = individualActionRoleParam.getAsString();
		}
		
		try {
			log.info("CALL service.internal().setPermissions()");
			ApiV2MessagesSetPermissionsRequestMessage msg = new ApiV2MessagesSetPermissionsRequestMessage();
			msg.setFileId(fileId);
			msg.setAction(action);
			msg.setIds(ids);
			msg.setReplaceExistingPermissions(replaceExistingPermissions);
			msg.setIndividualActionRole(individualActionRole);
			ApiV2MessagesSetPermissionsResponseMessage resultMsg = service.internal().setPermissions(Pt4dConfig.API_KEY, Pt4dConfig.API_DOMAIN, Pt4dConfig.DEFAULT_USER_ID, msg).execute();
			log.info("CALL service.internal().setPermissions()");
			
			//Setting Response content, try to parse JSON, if not set as string
			if(outputVariableParam != null && !outputVariableParam.getAsString().isEmpty()) {
				String outputVariable = outputVariableParam.getAsString();
				log.info(String.format("Setting variable %s with value %s", outputVariable, resultMsg.toPrettyString()));
				
				JsonNode jsonResponse = null;
				ObjectMapper mapper = new ObjectMapper();
				try {
					jsonResponse = mapper.readTree(resultMsg.toString());
					execution.setVariable(outputVariable, jsonResponse);
				} catch (Exception e) {
					log.info(String.format("Cannot parse response in json %s", e.getMessage()));
					e.printStackTrace();
					execution.setVariable(outputVariable, resultMsg.toString());
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
			log.error(e.getMessage());
			throw new Pt4dException("Cannot set permissions");
		}
	}
}