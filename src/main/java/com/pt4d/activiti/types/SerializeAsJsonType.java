package com.pt4d.activiti.types;

import org.activiti.engine.impl.variable.ValueFields;
import org.activiti.engine.impl.variable.VariableType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

public class SerializeAsJsonType implements VariableType {
 
	private static Logger log = LoggerFactory.getLogger(SerializeAsJsonType.class);
 
    private static final String SEPARATOR = "=>";
    private static final int SERIALIZED_MAX_LENGTH = 44000; // Activiti default field length
 
    //private DcJsonMapper mapper = new DcJsonMapper();
    private Gson gson = new Gson();
 
    @Override
    public String getTypeName() {
        return "serializeAsJson";
    }
 
    @Override
    public boolean isCachable() {
        return true;
    }
 
    @Override
    public Object getValue(ValueFields valueFields) {
        try {
            String value = valueFields.getTextValue();
            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
            String className = this.getClass().getName();
            try {
                Class clazz = classLoader.loadClass(className);
                return gson.fromJson(value,  clazz);
                //return mapper.readValue(json, clazz);
            } catch (ClassNotFoundException cnfe) {
                throw new IllegalArgumentException("Cannot find class  " + className + ".", cnfe);
            }
        } catch (Exception e) {
            log.error("Cannot convert JSON to object: " + valueFields.getTextValue(), e);
            return null;
        }
    }
 
    @Override
    public void setValue(Object value, ValueFields valueFields) {
        if (null == value) {
            valueFields.setTextValue(""); // needed to allow removing variables
        } else {
            try {
                String serialized = gson.toJson(value);
                if (serialized.length() > SERIALIZED_MAX_LENGTH) {
                    throw new IllegalArgumentException("Serialized value for object of type " +
                            value.getClass().getName() + " is too long to store as JSON object.");
                }
                valueFields.setTextValue(serialized);
            } catch (Exception e) {
                log.error("Cannot convert " + value.getClass().getName() + " to (JSON) string: " + value, e);
            }
        }
    }
 
    @Override
    public boolean isAbleToStore(Object value) {
        return value instanceof SerializeAsJson;
    }
 
}