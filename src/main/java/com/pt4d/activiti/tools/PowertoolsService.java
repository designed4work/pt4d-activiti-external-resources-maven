package com.pt4d.activiti.tools;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.appspot.powertools_v2.powertools.Powertools;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import com.pt4d.activiti.listeners.executions.DefaultStart;
import com.pt4d.config.Pt4dConfig;

public class PowertoolsService {
	
	private static Logger log = LoggerFactory.getLogger(PowertoolsService.class);
	
	public static Powertools createPowertoolsService() {
		log.info("API_IS_LOCAL env : " + System.getenv("API_IS_LOCAL"));
		log.info("API_DOMAIN env : " + System.getenv("API_DOMAIN"));
		log.info("Build PT4D api service with url : " + Pt4dConfig.getApiUrl());
		
		return new Powertools.Builder(new NetHttpTransport(), new GsonFactory(), null)
		.setRootUrl(Pt4dConfig.getApiUrl())
		.build();
	}
}
