package com.pt4d.activiti.base;

import java.io.IOException;

import org.activiti.engine.delegate.BpmnError;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.JavaDelegate;
import org.activiti.engine.delegate.TaskListener;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pt4d.activiti.delegates.GetFile;
import com.pt4d.activiti.exceptions.Pt4dException;

public abstract class Pt4dTaskListeners implements TaskListener {

	private static Logger baseLog = LoggerFactory.getLogger(Pt4dTaskListeners.class);
	
	public void notify(DelegateTask delegateTask) {
		try {
			pt4dExecute(delegateTask);
		} catch(BpmnError e1) {
			baseLog.error(ExceptionUtils.getStackTrace(e1));
			throw e1;
		} catch(Exception e2) {
			baseLog.error(ExceptionUtils.getStackTrace(e2));
			throw new BpmnError("TECHINCAL_ERROR");
		}
	}
	
	public abstract void pt4dExecute(DelegateTask delegateTask) throws Pt4dException, IOException;

}
