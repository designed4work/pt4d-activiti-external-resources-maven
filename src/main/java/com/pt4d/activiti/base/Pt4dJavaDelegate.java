package com.pt4d.activiti.base;

import org.activiti.engine.delegate.BpmnError;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.Expression;
import org.activiti.engine.delegate.JavaDelegate;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.pt4d.activiti.delegates.GetFile;
import com.pt4d.activiti.exceptions.Pt4dException;

public abstract class Pt4dJavaDelegate implements JavaDelegate {

	private static Logger baseLog = LoggerFactory.getLogger(Pt4dJavaDelegate.class);
	
	private Expression params;
	protected JsonObject parameters;
	
	@Override
	public void execute(DelegateExecution execution) throws Pt4dException {
		try {
			String sParameters = (String) params.getValue(execution);
			baseLog.info("parameters: " + sParameters);
			
			JsonParser parser = new JsonParser();
			parameters = parser.parse(sParameters).getAsJsonObject();
		} catch(JsonParseException e) {
			throw new Pt4dException("Cannot parse input parameter " + (String) params.getValue(execution));
		}
		
		pt4dExecute(execution);
	}
	
	public abstract void pt4dExecute(DelegateExecution execution) throws Pt4dException;

}
