package com.pt4d.activiti.rest;

import java.io.IOException;

import org.activiti.engine.ActivitiIllegalArgumentException;
import org.activiti.engine.impl.variable.JsonType;
import org.activiti.rest.service.api.engine.variable.RestVariable;
import org.activiti.rest.service.api.engine.variable.RestVariableConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.mxgraph.util.svg.ParseException;
import com.pt4d.activiti.beans.Pt4dFile;
import com.pt4d.activiti.listeners.tasks.DefaultCreateTask;
import com.pt4d.activiti.types.Pt4dFileType;

public class Pt4dFileRestVariableConverter implements RestVariableConverter {

	private static Logger log = LoggerFactory.getLogger(Pt4dFileRestVariableConverter.class);

	private Gson gson = new Gson();

	@Override
	public String getRestTypeName() {
		return "Pt4dFile";
	}

	@Override
	public Class<?> getVariableType() {
		return Pt4dFile.class;
	}

	@Override
	public Object getVariableValue(RestVariable result) {
		if (result.getValue() != null) {
			if (!(result.getValue() instanceof String)) {
				throw new ActivitiIllegalArgumentException("Converter can only convert string to json");
			}
			try {
				return gson.toJsonTree(result.getValue());
			} catch (ParseException e) {
				throw new ActivitiIllegalArgumentException(
						"The given variable value is not a json: '" + result.getValue() + "'", e);
			}
		}
		return null;
	}

	@Override
	public void convertVariableValue(Object variableValue, RestVariable result) {
		if (variableValue != null) {
			if (!(variableValue instanceof Pt4dFile)) {
				throw new ActivitiIllegalArgumentException("Converter can only convert Pt4dFile");
			}
			result.setValue(((Pt4dFile) variableValue));
		} else {
			result.setValue(null);
		}
	}

}
