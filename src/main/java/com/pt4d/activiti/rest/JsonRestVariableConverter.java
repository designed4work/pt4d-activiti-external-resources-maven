package com.pt4d.activiti.rest;

import java.io.IOException;

import org.activiti.engine.ActivitiIllegalArgumentException;
import org.activiti.rest.service.api.engine.variable.RestVariable;
import org.activiti.rest.service.api.engine.variable.RestVariableConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.mxgraph.util.svg.ParseException;

public class JsonRestVariableConverter implements RestVariableConverter {

	private static Logger log = LoggerFactory.getLogger(JsonRestVariableConverter.class);

	private Gson gson = new Gson();

	@Override
	public String getRestTypeName() {
		return "json";
	}

	@Override
	public Class<?> getVariableType() {
		return JsonNode.class;
	}

	@Override
	public Object getVariableValue(RestVariable result) {
		if (result.getValue() != null) {
			if (!(result.getValue() instanceof String)) {
				throw new ActivitiIllegalArgumentException("Converter can only convert string to json");
			}
			try {
				String sJson = gson.toJson(result.getValue());

				ObjectMapper mapper = new ObjectMapper();
				JsonNode json = null;
				try {
					json = mapper.readTree(sJson);
				} catch (JsonProcessingException e) {
					log.error(e.getMessage());
					e.printStackTrace();
				} catch (IOException e) {
					log.error(e.getMessage());
					e.printStackTrace();
				}
				return json;
			} catch (ParseException e) {
				throw new ActivitiIllegalArgumentException(
						"The given variable value is not a date: '" + result.getValue() + "'", e);
			}
		}
		return null;
	}

	@Override
	public void convertVariableValue(Object variableValue, RestVariable result) {
		if (variableValue != null) {
			if (!(variableValue instanceof JsonNode)) {
				throw new ActivitiIllegalArgumentException("Converter can only convert JsonNode");
			}
			result.setValue(((JsonNode) variableValue));
		} else {
			result.setValue(null);
		}
	}

}
