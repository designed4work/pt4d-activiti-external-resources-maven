package com.pt4d.activiti.validation;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.activiti.bpmn.model.ActivitiListener;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.bpmn.model.EndEvent;
import org.activiti.bpmn.model.FlowElement;
import org.activiti.bpmn.model.FormProperty;
import org.activiti.bpmn.model.Process;
import org.activiti.bpmn.model.StartEvent;
import org.activiti.bpmn.model.UserTask;
import org.activiti.validation.ValidationError;
import org.activiti.validation.validator.ProcessLevelValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pt4d.activiti.listeners.executions.DefaultEnd;
import com.pt4d.activiti.listeners.executions.DefaultErrorEnd;
import com.pt4d.activiti.listeners.executions.DefaultStart;
import com.pt4d.activiti.listeners.tasks.DefaultCompleteTask;
import com.pt4d.activiti.listeners.tasks.DefaultCreateTask;
import com.pt4d.activiti.listeners.tasks.SetOrganizerAsCandidate;
import com.pt4d.activiti.listeners.tasks.SetOwnerAsCandidate;
import com.pt4d.activiti.listeners.tasks.SetReaderAsCandidate;
import com.pt4d.activiti.listeners.tasks.SetWriterAsCandidate;

public class DefaultPt4dElementValidator extends ProcessLevelValidator {
	
	private static Logger log = LoggerFactory.getLogger(DefaultPt4dElementValidator.class);
	
	private static final String[] ROLE_AS_CANDIDATE_LISTENERS = {
			SetOwnerAsCandidate.class.getName(),
			SetWriterAsCandidate.class.getName(),
			SetReaderAsCandidate.class.getName(),
			SetOrganizerAsCandidate.class.getName()
	};
	
	private static final String[] DEFAULT_USER_TASK_CREATE_LISTENERS = {
			DefaultCreateTask.class.getName()
	};
	
	private static final String[] DEFAULT_USER_TASK_COMPLETE_LISTENERS = {
			DefaultCompleteTask.class.getName()
	};
	
	private static final String[] DEFAULT_START_LISTENERS = {
			DefaultStart.class.getName()
	};
	
	private static final String[] DEFAULT_END_LISTENERS = {
			DefaultEnd.class.getName(),
			DefaultErrorEnd.class.getName()
	};

	@Override
	protected void executeValidation(BpmnModel bpmnModel, Process process, List<ValidationError> errors) {
		log.info(String.format("PT4D Validation for process %s", process.getId()));
		
		//Validate candidate starters. somone has to be able to start the process
		this.validatProcessStarterCandidates(process, bpmnModel.getTargetNamespace(), errors);
		
		//Validate listeners on start event
		List<StartEvent> startEvents = process.findFlowElementsOfType(StartEvent.class, false);
		this.validateListeners(startEvents, DEFAULT_START_LISTENERS, "start", errors);
		
		//Validate listeners on end event
		List<EndEvent> endEvents = process.findFlowElementsOfType(EndEvent.class, false);
		this.validateListeners(endEvents, DEFAULT_END_LISTENERS, "end", errors);
		
		//Validate listeners on user tasks
		List<UserTask> userTasks = process.findFlowElementsOfType(UserTask.class, false);
		this.validateUserTaskListeners(userTasks, DEFAULT_USER_TASK_CREATE_LISTENERS, "create", errors);
		this.validateUserTaskListeners(userTasks, DEFAULT_USER_TASK_COMPLETE_LISTENERS, "complete", errors);
		
		//Validate someone can submit the tasks
		this.validateUserTasksCandidates(userTasks, process, errors);
		
		//Validate the startEvent has a form with at least the submit button
		this.validateStartEventForm(startEvents, process, errors);
		
		//Validate the task has a form with at least the submit button
		this.validateUserTaskForm(userTasks, process, errors);
	}
	
	protected void validateListeners(List<? extends FlowElement> flowElements, String [] implementations, String event, List<ValidationError> errors) {
		for (FlowElement flowElement : flowElements) {
			boolean present = false;
			for (Iterator<ActivitiListener> it = flowElement.getExecutionListeners().iterator(); it.hasNext(); ) {
				ActivitiListener activitiListener = it.next();
				if(activitiListener.getEvent().equals(event) && Arrays.asList(implementations).contains(activitiListener.getImplementation())) {
					present = true;
				}
			}
			
			if (!present) {
				ValidationError error = new ValidationError();
				error.setProblem(String.format("PT4D validation: %s not found on flowElement %s for event %s", String.join(", ", Arrays.asList(implementations)), flowElement.getId(), event));
				error.setActivityId(flowElement.getId());
				error.setActivityName(flowElement.getName());
				errors.add(error);	
			}
		}
	}
	
	protected void validatProcessStarterCandidates(Process process, String targetNamespace, List<ValidationError> errors) {
		if (process.getCandidateStarterUsers().size() == 0 && process.getCandidateStarterGroups().size() == 0) {
			if (targetNamespace == null || !targetNamespace.contains("writer_can_start") && !targetNamespace.contains("reader_can_start") && !targetNamespace.contains("owner_can_start")) {
				ValidationError error = new ValidationError();
				error.setProblem(String.format("PT4D validation: no candidate starter found on process %s and no dynamic rights either. No one will be able to start the workflow", process.getId()));
				error.setProcessDefinitionId(process.getId());
				error.setProcessDefinitionName(process.getName());
				errors.add(error);	
			}
		}
	}

	protected void validateUserTaskListeners(List<UserTask> userTasks, String [] implementations, String event, List<ValidationError> errors) {
		for (UserTask userTask : userTasks) {
			boolean present = false;
			for (Iterator<ActivitiListener> it = userTask.getTaskListeners().iterator(); it.hasNext(); ) {
				ActivitiListener activitiListener = it.next();
				if(activitiListener.getEvent().equals(event) && Arrays.asList(implementations).contains(activitiListener.getImplementation())) {
					present = true;
				}
			}
			
			if (!present) {
				ValidationError error = new ValidationError();
				error.setProblem(String.format("PT4D validation: %s not found on flowElement %s for event %s", String.join(", ", Arrays.asList(implementations)), userTask.getId(), event));
				error.setActivityId(userTask.getId());
				error.setActivityName(userTask.getName());
				errors.add(error);	
			}
		}
	}

	protected void validateUserTasksCandidates(List<UserTask> userTasks, Process process, List<ValidationError> errors) {
		for (UserTask userTask : userTasks) {
			if (userTask.getCandidateUsers().size() == 0 && userTask.getCandidateGroups().size() == 0) {
				this.validateUserTaskListeners(Arrays.asList(userTask), ROLE_AS_CANDIDATE_LISTENERS, "create", errors);
			}
		}
	}
	
	protected void validateStartEventForm(List<StartEvent> startEvents, Process process, List<ValidationError> errors) {
		for (StartEvent startEvent : startEvents) {
			boolean present = false;
			for (FormProperty formProperty : startEvent.getFormProperties()) {
				if (formProperty.getType() == null  ||  formProperty.getId() == null) {
					ValidationError error = new ValidationError();
					error.setProblem(String.format("PT4D validation: formProperty with no id or no type found for startEvent %s", startEvent.getId()));
					error.setActivityId(startEvent.getId());
					error.setActivityName(startEvent.getName());
					errors.add(error);
				}
				if (formProperty.getType().equals("enum") && formProperty.getId().equals("btn_submit")) {
					present = true;
					if (formProperty.getFormValues().size() <= 0) {
						ValidationError error = new ValidationError();
						error.setProblem(String.format("PT4D validation: formProperty with id=btn_submit found for startEvent %s but no there is no value in it", startEvent.getId()));
						error.setActivityId(startEvent.getId());
						error.setActivityName(startEvent.getName());
						errors.add(error);
					}
				}
			}
			
			if (!present) {
				ValidationError error = new ValidationError();
				error.setProblem(String.format("PT4D validation: formProperty with id=btn_submit and type=enum not found on startEvent %s", startEvent.getId()));
				error.setActivityId(startEvent.getId());
				error.setActivityName(startEvent.getName());
				errors.add(error);	
			}
		}
	}
	
	protected void validateUserTaskForm(List<UserTask> userTasks, Process process, List<ValidationError> errors) {
		for (UserTask userTask : userTasks) {
			boolean present = false;
			for (FormProperty formProperty : userTask.getFormProperties()) {
				if (formProperty.getType() == null  ||  formProperty.getId() == null) {
					ValidationError error = new ValidationError();
					error.setProblem(String.format("PT4D validation: formProperty with no id or no type found for userTask %s", userTask.getId()));
					error.setActivityId(userTask.getId());
					error.setActivityName(userTask.getName());
					errors.add(error);
				}
				if (formProperty.getType().equals("enum") && formProperty.getId().equals("btn_submit")) {
					present = true;
					if (formProperty.getFormValues().size() <= 0) {
						ValidationError error = new ValidationError();
						error.setProblem(String.format("PT4D validation: formProperty with id=btn_submit found for userTask %s but no there is no value in it", userTask.getId()));
						error.setActivityId(userTask.getId());
						error.setActivityName(userTask.getName());
						errors.add(error);
					}
				}
			}
			
			if (!present) {
				ValidationError error = new ValidationError();
				error.setProblem(String.format("PT4D validation: formProperty with id=btn_submit and type=enum not found on userTask %s", userTask.getId()));
				error.setActivityId(userTask.getId());
				error.setActivityName(userTask.getName());
				errors.add(error);	
			}
		}
	}
}
