package com.pt4d.activiti.delegates;

import java.io.IOException;

import org.activiti.engine.RuntimeService;
import org.activiti.engine.delegate.BpmnError;
import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.Expression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.appspot.powertools_v2.powertools.Powertools;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import com.pt4d.activiti.base.Pt4dJavaDelegate;
import com.pt4d.activiti.tools.PowertoolsService;
import com.pt4d.config.Pt4dConfig;

public class GetFile extends Pt4dJavaDelegate {

	private static Logger log = LoggerFactory.getLogger(GetFile.class);
	private static Powertools service = PowertoolsService.createPowertoolsService();
	
	@Autowired
	RuntimeService runtimeService;

	private Expression fileIdExpression;

	@Override
	public void pt4dExecute(DelegateExecution execution) {
		execution.getCurrentActivityId();
		try {
			String fileId = parameters.get("fileId").getAsString();
			
			log.info("Getting file " + fileId);
			
			service.files().get(Pt4dConfig.API_KEY, Pt4dConfig.API_DOMAIN, Pt4dConfig.DEFAULT_USER_ID, fileId).execute();
		} catch (IOException e) {
			log.error("Cannot get file.", e);
			throw new BpmnError("FILE_NOT_FOUND");
		}
	}
}