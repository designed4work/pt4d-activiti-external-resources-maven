package com.pt4d.activiti.formproperties;

import org.activiti.engine.form.AbstractFormType;

public class UserType extends AbstractFormType {

	public static final String TYPE_NAME = "user";

	public String getName() {
		return TYPE_NAME;
	}

	public Object convertFormValueToModelValue(String propertyValue) {
		return propertyValue;
	}

	public String convertModelValueToFormValue(Object modelValue) {
		if (modelValue == null) {
			return null;
		}
		return modelValue.toString();
	}

}