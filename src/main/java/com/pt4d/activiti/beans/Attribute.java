package com.pt4d.activiti.beans;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.appspot.powertools_v2.powertools.model.ApiV2MessagesAttributeMessage;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesFileMessage;

public class Attribute {
	
	private String fileType;
	private String id;
	private String defaultValue;
	private String name;
	private String format;
	private String status;
	
	public String getFileType() {
		return fileType;
	}
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDefaultValue() {
		return defaultValue;
	}
	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFormat() {
		return format;
	}
	public void setFormat(String format) {
		this.format = format;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getDefaultValueAsString() {
		return (String) this.defaultValue;
	}
	public Long getDefaultValueAsLong() {
		return Long.parseLong(this.defaultValue);
	}
	public Double getDefaultValueAsDouble() {
		return Double.parseDouble(this.defaultValue);
	}
	public Boolean getDefaultValueAsBoolean() {
		return Boolean.parseBoolean(this.defaultValue);
	}
	public List<String> getDefaultValueAsStringList() {
		List<String> items = Arrays.asList(this.defaultValue.split("\\s*,\\s*"));
		return items;
	}
	
	public static Attribute fromMessage(ApiV2MessagesAttributeMessage msg) {
		Attribute attribute = new Attribute();
		attribute.setId(msg.getId());
		attribute.setDefaultValue(msg.getDefaultValue());
		attribute.setName(msg.getName());
		attribute.setFormat(msg.getFormat());
		attribute.setStatus(msg.getStatus());
		return attribute;
	}
	
}
