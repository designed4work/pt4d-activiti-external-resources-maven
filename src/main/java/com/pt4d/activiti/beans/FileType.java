package com.pt4d.activiti.beans;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.appspot.powertools_v2.powertools.model.ApiV2MessagesAttributeMessage;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesFileTypeMessage;

public class FileType {

	private Long id;
	private String name;
	private String description;
	
	private List<Attribute> attributes;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Attribute> getAttributes() {
		return attributes;
	}

	public void setAttributes(List<Attribute> attributes) {
		this.attributes = attributes;
	}
	
	public static FileType fromMessage(ApiV2MessagesFileTypeMessage msg) {
		FileType fileType = new FileType();
		fileType.setId(msg.getId());
		fileType.setName(msg.getName());
		fileType.setDescription(msg.getDescription());
		
		List<Attribute> attributes = new ArrayList<Attribute>();
		if (msg.getAttributes() != null) {
			for (Iterator<ApiV2MessagesAttributeMessage> it = msg.getAttributes().iterator(); it.hasNext();) {
				attributes.add(Attribute.fromMessage(it.next()));
			}
		}
		
		return fileType;
	}
}
