package com.pt4d.activiti.beans;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.appspot.powertools_v2.powertools.model.ApiV2MessagesAttributeMessage;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesFileTypeMessage;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesTagMessage;

public class Tag {
	
	private Long id;
	private String value;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	public static Tag fromMessage(ApiV2MessagesTagMessage msg) {
		Tag tag = new Tag();
		tag.setId(msg.getId());
		tag.setValue(msg.getValue());
		return tag;
	}
	
}
