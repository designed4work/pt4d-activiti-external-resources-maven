package com.pt4d.activiti.beans;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.appspot.powertools_v2.powertools.model.ApiV2MessagesAttributeMessage;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesFileTypeMessage;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesWorkspaceMessage;

public class Workspace {
	private Long id;
	private String name;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public static Workspace fromMessage(ApiV2MessagesWorkspaceMessage msg) {
		Workspace workspace = new Workspace();
		workspace.setId(msg.getId());
		workspace.setName(msg.getName());
		return workspace;
	}
}
