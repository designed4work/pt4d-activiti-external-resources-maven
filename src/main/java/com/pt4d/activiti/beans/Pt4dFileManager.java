package com.pt4d.activiti.beans;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.appspot.powertools_v2.powertools.Powertools;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesFileMessage;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import com.pt4d.activiti.delegates.GetFile;
import com.pt4d.activiti.tools.CrunchifyInMemoryCache;
import com.pt4d.activiti.tools.PowertoolsService;
import com.pt4d.config.Pt4dConfig;

public class Pt4dFileManager {
	
	private static Logger log = LoggerFactory.getLogger(Pt4dFileManager.class);
	private static Powertools service = PowertoolsService.createPowertoolsService();
	
	private CrunchifyInMemoryCache<String, ApiV2MessagesFileMessage> pt4dFileCache;
	
	public Pt4dFileManager() {
		this.pt4dFileCache = new CrunchifyInMemoryCache<String, ApiV2MessagesFileMessage>(60, 5, 100);		
	}
	
	public ApiV2MessagesFileMessage getFileMessageById(String fileId) throws IOException {
		boolean fromCache=true;
		ApiV2MessagesFileMessage fileMessage = pt4dFileCache.get(fileId);
		if(fileMessage==null) {
			fileMessage = service.files().get(Pt4dConfig.API_KEY, Pt4dConfig.API_DOMAIN, Pt4dConfig.DEFAULT_USER_ID, fileId).setWithDriveData(true).execute();
			pt4dFileCache.put(fileId, fileMessage);
		}
		log.info("Retrieve file " + fileId + " , from cache : " + fromCache);
		return fileMessage;
	}
	
	public static Pt4dFile getFileById(String fileId) throws IOException {
		ApiV2MessagesFileMessage file = service.files().get(Pt4dConfig.API_KEY, Pt4dConfig.API_DOMAIN, Pt4dConfig.DEFAULT_USER_ID, fileId).setWithDriveData(true).execute();
		Pt4dFile pt4dFile = Pt4dFile.fromMessage(file);
		return pt4dFile;
	}

}
