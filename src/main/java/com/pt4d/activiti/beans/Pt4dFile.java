package com.pt4d.activiti.beans;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.appspot.powertools_v2.powertools.model.ApiV2MessagesFileAttributeMessage;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesFileMessage;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesTagMessage;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.pt4d.activiti.types.SerializeAsJson;

public class Pt4dFile {

	private static Logger log = LoggerFactory.getLogger(Pt4dFile.class);
	
	private String id;
    private String title;
    private String alternateLink;
    private String iconLink;
    
    private Workspace workspace;
    private FileType fileType;
    
    private List<FileAttribute> attributes;
    private List<Tag> tags;
	
	public static Pt4dFile fromMessage(ApiV2MessagesFileMessage fileMessage) {
		Pt4dFile file = new Pt4dFile();
		
		//ID
		file.setId(fileMessage.getId());

		//Title from drive data
		JsonObject driveDataJson = (new JsonParser()).parse(fileMessage.getDriveData()).getAsJsonObject();
		file.setTitle(driveDataJson.get("title").getAsString());
		
		//AlternaleLink from drive data
		file.setAlternateLink(driveDataJson.get("alternateLink").getAsString());
		
		//IconLink from drive data
		file.setIconLink(driveDataJson.get("iconLink").getAsString());
		
		//Workspace
		Workspace workspace = null;
		if (fileMessage.getWorkspace() != null) {
			workspace = Workspace.fromMessage(fileMessage.getWorkspace());
		}
		file.setWorkspace(workspace);
		
		//File Type
		FileType fileType = null;
		if (fileMessage.getFileType() != null) {
			fileType = FileType.fromMessage(fileMessage.getFileType());
		}
		file.setFileType(fileType);
		
		//Tags
		List<Tag> tags = new ArrayList();
		if (fileMessage.getTags() != null) {
			for (Iterator<ApiV2MessagesTagMessage> it = fileMessage.getTags().iterator(); it.hasNext();) {
				ApiV2MessagesTagMessage elem = it.next();
				tags.add(Tag.fromMessage(elem));
			}
		}
		file.setTags(tags);
		
		//Attributes
		List<FileAttribute> attributes = new ArrayList();
		if (fileMessage.getAttributes() != null) {
			for (Iterator<ApiV2MessagesFileAttributeMessage> it = fileMessage.getAttributes().iterator(); it.hasNext();) {
				ApiV2MessagesFileAttributeMessage elem = it.next();
				attributes.add(FileAttribute.fromMessage(elem));
			}
		}
		file.setAttributes(attributes);
		
		Gson gson = new Gson();
		System.out.println(gson.toJson(file));
		
		return file;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getIconLink() {
		return iconLink;
	}

	public void setIconLink(String iconLink) {
		this.iconLink = iconLink;
	}

	public Workspace getWorkspace() {
		return workspace;
	}

	public void setWorkspace(Workspace workspace) {
		this.workspace = workspace;
	}

	public FileType getFileType() {
		return fileType;
	}

	public void setFileType(FileType fileType) {
		this.fileType = fileType;
	}

	public List<FileAttribute> getAttributes() {
		return attributes;
	}

	public void setAttributes(List<FileAttribute> attributes) {
		this.attributes = attributes;
	}

	public List<Tag> getTags() {
		return tags;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}

	public String getAlternateLink() {
		return alternateLink;
	}

	public void setAlternateLink(String alternateLink) {
		this.alternateLink = alternateLink;
	}
    
	//Tool functions
	public Boolean hasTag(String value) {		
		if(this.getTags().contains(value)) {
			return true;
		}
		return false;
	}
	
	public FileAttribute getAttributeByName(String name) {
		for (Iterator<FileAttribute> it = this.getAttributes().iterator(); it.hasNext();) {
			FileAttribute fileAttribute = it.next();
			if(fileAttribute.getName().equals(name)) {
				return fileAttribute;
			}
		}
		return null;
	}
	
	public String getAttributeValueByName(String name) {
		FileAttribute fileAttribute = this.getAttributeByName(name);
		if(fileAttribute != null) {
			return fileAttribute.getValue();
		}
		return null;
	}
	
	public FileAttribute getAttributeById(Long id) {
		String sId = id.toString();
		for (Iterator<FileAttribute> it = this.getAttributes().iterator(); it.hasNext();) {
			FileAttribute fileAttribute = it.next();
			if(fileAttribute.getId().equals(id) || fileAttribute.getId().equals(sId)) {
				return fileAttribute;
			}
		}
		return null;
	}
	
	public String getAttributeValueById(Long id) {
		FileAttribute fileAttribute = this.getAttributeById(id);
		if(fileAttribute != null) {
			return fileAttribute.getValue();
		}
		return null;
	}

	public FileAttribute getAttributeByFileTypeAttributeId(Long id) {
		String sId = id.toString();
		for (Iterator<FileAttribute> it = this.getAttributes().iterator(); it.hasNext();) {
			FileAttribute fileAttribute = it.next();
			if(fileAttribute.getFileTypeAttributeId().equals(id) || fileAttribute.getFileTypeAttributeId().equals(sId)) {
				return fileAttribute;
			}
		}
		return null;
	}
	
	public String getAttributeValueByFileTypeAttributeId(Long id) {
		FileAttribute fileAttribute = this.getAttributeByFileTypeAttributeId(id);
		if(fileAttribute != null) {
			return fileAttribute.getValue();
		}
		return null;
	}
	 
}