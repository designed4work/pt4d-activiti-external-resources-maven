package com.pt4d.activiti.beans;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.appspot.powertools_v2.powertools.model.ApiV2MessagesFileAttributeMessage;
import com.google.api.client.util.Strings;
import com.pt4d.activiti.exceptions.Pt4dException;
import com.pt4d.activiti.rest.Pt4dFileRestVariableConverter;
import com.pt4d.config.Pt4dConstants;

public class FileAttribute {
	
	private static Logger log = LoggerFactory.getLogger(Pt4dFile.class);
	
	private String name;
	private Long id;
	private String fileTypeAttributeId;
	private String value;
	private String format;

	//Getter Setters
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getFileTypeAttributeId() {
		return fileTypeAttributeId;
	}

	public void setFileTypeAttributeId(String fileTypeAttributeId) {
		this.fileTypeAttributeId = fileTypeAttributeId;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public static FileAttribute fromMessage(ApiV2MessagesFileAttributeMessage msg) {
		FileAttribute attribute = new FileAttribute();
		attribute.setId(msg.getId());
		attribute.setValue(msg.getValue());
		if(msg.getAttribute() != null) {
			attribute.setName(msg.getAttribute().getName());
			attribute.setFileTypeAttributeId(msg.getAttribute().getId());
			attribute.setFormat(msg.getAttribute().getFormat());
		}
		return attribute;
	}
	
	public Boolean isEmpty() {
		if(this.getValue() == null || this.getValue().compareTo("") == 0) {
			return true;
		}
		return false;
	}
	
	//Casts
	public String asString() {
		return this.value;
	}
	public Boolean asBoolean() {
		return Boolean.parseBoolean(this.value);
	}
	public Long asLong() throws ParseException {
		return Long.parseLong(this.value);
	}
	public Float asFloat() throws ParseException {
		return Float.parseFloat(this.value);
	}
	public Date asDate() throws ParseException {
		try {
			DateFormat df = new SimpleDateFormat();
			return df.parse(this.value);
		} catch(ParseException e) {
			DateFormat df2 = new SimpleDateFormat(Pt4dConstants.PT4D_DATE_FORMAT);
			return df2.parse(this.value);
		}
	}
	public List<String> asStringList() throws ParseException {
		String[] arr = this.value.split(",");
		List<String> values = new ArrayList<String>();
		for (String v : arr) {
			values.add(v.trim());
		}
		return values;
	}
	
	public Boolean compareTo(String value, String method) throws Pt4dException, ParseException {
		//Manage empty values
		if(this.isEmpty() || Strings.isNullOrEmpty(value)) {
			if(method.equals("eq")) {
				return this.isEmpty() == Strings.isNullOrEmpty(value);
			} else if (method.equals("neq")) {
				return this.isEmpty() != Strings.isNullOrEmpty(value);
			} else if (method.equals("gt") || method.equals("gte") || method.equals("lt") || method.equals("lte")) {
				throw new Pt4dException(String.format("Cannot compare empty values with method %s for attribute %s", method, this.getName()));
			}
		}
		
		//Manage non empty values
		if (this.getFormat().equals("free")) {
			if (method.equals("eq")) {
				return this.asString().compareTo(value) == 0 ? true : false;
			} else if (method.equals("neq")) {
				return this.asString().compareTo(value) != 0 ? true : false;
			} else if (method.equals("gt")) {
				return this.asString().compareTo(value) > 0 ? true : false;
			} else if (method.equals("gte")) {
				return this.asString().compareTo(value) >= 0 ? true : false;
			} else if (method.equals("lt")) {
				return this.asString().compareTo(value) < 0 ? true : false;
			} else if (method.equals("lte")) {
				return this.asString().compareTo(value) <= 0 ? true : false;
			} else if (method.equals("ctn")) {
				return this.asString().contains(value);
			} else if (method.equals("nctn")) {
				return !this.asString().contains(value);
			} else {
				throw new Pt4dException("Unkwnown method " + method);
			}
		} else if (this.getFormat().equals("number")) {
			Float parsedValue = Float.parseFloat(value);
			if (method.equals("eq")) {
				return this.asFloat().compareTo(parsedValue) == 0 ? true : false;
			} else if (method.equals("neq")) {
				return this.asFloat().compareTo(parsedValue) != 0 ? true : false;
			} else if (method.equals("gt")) {
				return this.asFloat().compareTo(parsedValue) > 0 ? true : false;
			} else if (method.equals("gte")) {
				return this.asFloat().compareTo(parsedValue) >= 0 ? true : false;
			} else if (method.equals("lt")) {
				return this.asFloat().compareTo(parsedValue) < 0 ? true : false;
			} else if (method.equals("lte")) {
				return this.asFloat().compareTo(parsedValue) <= 0 ? true : false;
			} else {
				throw new Pt4dException("Unkwnown method " + method);
			}
		} else if (this.getFormat().equals("bool")) {
			Boolean parsedValue = Boolean.parseBoolean(value);
			if (method.equals("eq")) {
				return this.asBoolean().compareTo(parsedValue) == 0 ? true : false;
			} else if (method.equals("neq")) {
				return this.asBoolean().compareTo(parsedValue) != 0 ? true : false;
			} else {
				throw new Pt4dException("Compare not supported for format bool and method " + method);
			}
		} else if (this.getFormat().equals("date")) {
			Date parsedValue = null;
			try {
				DateFormat df = new SimpleDateFormat();
				parsedValue = df.parse(value);
			} catch(ParseException e) {
				DateFormat df2 = new SimpleDateFormat(Pt4dConstants.PT4D_DATE_FORMAT);
				parsedValue = df2.parse(value);
			}
			if (method.equals("eq")) {
				return this.asDate().compareTo(parsedValue) == 0 ? true : false;
			} else if (method.equals("neq")) {
				return this.asDate().compareTo(parsedValue) != 0 ? true : false;
			} else if (method.equals("gt")) {
				return this.asDate().compareTo(parsedValue) > 0 ? true : false;
			} else if (method.equals("gte")) {
				return this.asDate().compareTo(parsedValue) >= 0 ? true : false;
			} else if (method.equals("lt")) {
				return this.asDate().compareTo(parsedValue) < 0 ? true : false;
			} else if (method.equals("lte")) {
				return this.asDate().compareTo(parsedValue) <= 0 ? true : false;
			} else {
				throw new Pt4dException("Unkwnown method " + method);
			}
		} else if (this.getFormat().equals("list")) {
			if (method.equals("eq")) {
				return this.asString().compareTo(value) == 0 ? true : false;
			} else if (method.equals("neq")) {
				return this.asString().compareTo(value) != 0 ? true : false;
			} else {
				throw new Pt4dException("Unkwnown method " + method);
			}
		} else if (this.getFormat().equals("mlist")) {
			List<String> values = this.asStringList();
			if (method.equals("ctn")) {
				return values.contains(value);
			} else if (method.equals("nctn")) {
				return !values.contains(value);
			} else {
				throw new Pt4dException("Unkwnown method " + method);
			}
		} else {
			throw new Pt4dException("Unknwown attribute format " + this.getFormat());
		}
	}
	
	public Boolean _eq(String value) throws Pt4dException, ParseException {
		log.info(String.format("Compare _eq attribute %s (%s) with value %s to value %s", this.getName(), this.getFormat(), this.getValue(), value));
		Boolean result = this.compareTo(value, "eq");
		log.info(result.toString());
		return result;
	}
	public Boolean _neq(String value) throws Pt4dException, ParseException {
		log.info(String.format("Compare _neq attribute %s (%s) with value %s to value %s", this.getName(), this.getFormat(), this.getValue(), value));
		Boolean result = this.compareTo(value, "neq");
		log.info(result.toString());
		return result;
	}
	public Boolean _gt(String value) throws Pt4dException, ParseException {
		log.info(String.format("Compare _gt attribute %s (%s) with value %s to value %s", this.getName(), this.getFormat(), this.getValue(), value));
		Boolean result = this.compareTo(value, "gt");
		log.info(result.toString());
		return result;
	}
	public Boolean _gte(String value) throws Pt4dException, ParseException {
		log.info(String.format("Compare _gte attribute %s (%s) with value %s to value %s", this.getName(), this.getFormat(), this.getValue(), value));
		Boolean result = this.compareTo(value, "gte");
		log.info(result.toString());
		return result;
	}
	public Boolean _lt(String value) throws Pt4dException, ParseException {
		log.info(String.format("Compare _lt attribute %s (%s) with value %s to value %s", this.getName(), this.getFormat(), this.getValue(), value));
		Boolean result =  this.compareTo(value, "lt");
		log.info(result.toString());
		return result;
	}
	public Boolean _lte(String value) throws Pt4dException, ParseException {
		log.info(String.format("Compare _lte attribute %s (%s) with value %s to value %s", this.getName(), this.getFormat(), this.getValue(), value));
		Boolean result = this.compareTo(value, "lte");
		log.info(result.toString());
		return result;
	}
	public Boolean _ctn(String value) throws Pt4dException, ParseException {
		log.info(String.format("Compare _ctn attribute %s (%s) with value %s to value %s", this.getName(), this.getFormat(), this.getValue(), value));
		Boolean result = this.compareTo(value, "ctn");
		log.info(result.toString());
		return result;
	}
	public Boolean _nctn(String value) throws Pt4dException, ParseException {
		log.info(String.format("Compare _nctn attribute %s (%s) with value %s to value %s", this.getName(), this.getFormat(), this.getValue(), value));
		Boolean result = this.compareTo(value, "nctn");
		log.info(result.toString());
		return result;
	}
	
}
