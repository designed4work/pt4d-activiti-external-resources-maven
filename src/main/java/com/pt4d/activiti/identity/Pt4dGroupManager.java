package com.pt4d.activiti.identity;	

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.activiti.engine.ActivitiException;
import org.activiti.engine.delegate.event.ActivitiEventType;
import org.activiti.engine.delegate.event.impl.ActivitiEventBuilder;
import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.GroupQuery;
import org.activiti.engine.impl.GroupQueryImpl;
import org.activiti.engine.impl.Page;
import org.activiti.engine.impl.context.Context;
import org.activiti.engine.impl.db.DbSqlSession;
import org.activiti.engine.impl.db.PersistentObject;
import org.activiti.engine.impl.interceptor.CommandContext;
import org.activiti.engine.impl.persistence.entity.GroupEntity;
import org.activiti.engine.impl.persistence.entity.GroupEntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.appspot.powertools_v2.powertools.Powertools;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesGroupMessage;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesListOfGroupsMessage;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import com.pt4d.activiti.caches.GroupsByUserCache;
import com.pt4d.activiti.tools.PowertoolsService;
import com.pt4d.config.Pt4dConfig;

public class Pt4dGroupManager extends GroupEntityManager {

	private static Logger log = LoggerFactory.getLogger(Pt4dGroupManager.class);
	private static Powertools service = PowertoolsService.createPowertoolsService();

	public Group createNewGroup(String groupId) {
		throw new ActivitiException("PT4D group manager doesn't support creating a group");
	}

	public void insertGroup(Group group) {
		throw new ActivitiException("PT4D group manager doesn't support inserting a group");
	}

	public void updateGroup(Group updatedGroup) {
		throw new ActivitiException("PT4D group manager doesn't support updating a group");
	}

	public void deleteGroup(String groupId) {
		throw new ActivitiException("PT4D group manager doesn't support deleting a group");
	}

	public GroupQuery createNewGroupQuery() {
		//log.info("--------------------------------->>> createNewGroupQuery");
		return super.createNewGroupQuery();
	}

	@SuppressWarnings("unchecked")
	public List<Group> findGroupByQueryCriteria(GroupQueryImpl query, Page page) {
		//log.info("List<Group> findGroupByQueryCriteria(GroupQueryImpl query, Page page)");
		List<Group> list = new ArrayList<Group>();

		if (query.getId() != null)
			//log.info(">>> criteria: groupId " + query.getId());
		if (query.getUserId() != null)
			//log.info(">>> criteria: userId " + query.getUserId());
		if (query.getName() != null)
			//log.info(">>> criteria: name " + query.getName());
		if (query.getNameLike() != null)
			//log.info(">>> criteria: nameLike " + query.getNameLike());
		if (query.getType() != null)
			//log.info(">>> criteria: type " + query.getType());

		if (query.getName() != null || query.getNameLike() != null || query.getType() != null) {
			throw new ActivitiException("Not implemented group search query criteria");
		}

		if (query.getId() != null) {
			try {
				ApiV2MessagesGroupMessage pt4dGroup = service.groups()
						.get(Pt4dConfig.API_KEY, Pt4dConfig.API_DOMAIN, Pt4dConfig.DEFAULT_USER_ID, query.getId())
						.execute();

				Group g = new GroupEntity(pt4dGroup.getId());
				g.setName(pt4dGroup.getEmail());
				list.add(g);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if (query.getUserId() != null) {
			list = findGroupsByUser(query.getUserId());
		}

		//log.info(">>> " + list.size() + " groups found");
		return list;
	}

	public long findGroupCountByQueryCriteria(GroupQueryImpl query) {
		//log.info("long findGroupCountByQueryCriteria(GroupQueryImpl query)");
		return super.findGroupCountByQueryCriteria(query);
	}

	@SuppressWarnings("unchecked")
	public List<Group> findGroupsByUser(String userId) {
		//log.info("List<Group> findGroupsByUser(String userId)");
		//log.info(">>> userId " + userId);
		List<Group> list = new ArrayList<Group>();
		
		ApiV2MessagesListOfGroupsMessage groupList = GroupsByUserCache.getInstance().get(userId);
		try {
			if (groupList == null) {
				log.info("List<Group> findGroupsByUser(String userId) => No cache found, calling pt4d api ...");
				groupList = service.users()
						.getGroups(Pt4dConfig.API_KEY, Pt4dConfig.API_DOMAIN, Pt4dConfig.DEFAULT_USER_ID, userId).execute();
				GroupsByUserCache.getInstance().put(userId, groupList);
			}
			
			if (groupList != null && groupList.getGroups() != null) {
				for (ApiV2MessagesGroupMessage group : groupList.getGroups()) {
					Group g = new GroupEntity(group.getId());
					g.setName(group.getEmail());
					list.add(g);
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		//log.info(">>> " + list.size() + " groups found");
		return list;
	}

	@SuppressWarnings("unchecked")
	public List<Group> findGroupsByNativeQuery(Map<String, Object> parameterMap, int firstResult, int maxResults) {
		throw new ActivitiException("PT4D group manager doesn't support querying a group");
	}

	public long findGroupCountByNativeQuery(Map<String, Object> parameterMap) {
		throw new ActivitiException("PT4D group manager doesn't support querying a group");
	}

	@Override
	public boolean isNewGroup(Group group) {
		throw new ActivitiException("PT4D group manager doesn't support creating a group");
	}

}
