package com.pt4d.activiti.identity;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.activiti.engine.ActivitiException;
import org.activiti.engine.ActivitiObjectNotFoundException;
import org.activiti.engine.delegate.event.ActivitiEventType;
import org.activiti.engine.delegate.event.impl.ActivitiEventBuilder;
import org.activiti.engine.identity.Group;
import org.activiti.engine.identity.Picture;
import org.activiti.engine.identity.User;
import org.activiti.engine.identity.UserQuery;
import org.activiti.engine.impl.Page;
import org.activiti.engine.impl.UserQueryImpl;
import org.activiti.engine.impl.context.Context;
import org.activiti.engine.impl.db.DbSqlSession;
import org.activiti.engine.impl.db.PersistentObject;
import org.activiti.engine.impl.interceptor.CommandContext;
import org.activiti.engine.impl.persistence.entity.GroupEntity;
import org.activiti.engine.impl.persistence.entity.IdentityInfoEntity;
import org.activiti.engine.impl.persistence.entity.UserEntity;
import org.activiti.engine.impl.persistence.entity.UserEntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.appspot.powertools_v2.powertools.Powertools;
import com.appspot.powertools_v2.powertools.Powertools.Users;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesGroupMessage;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesListOfGroupsMessage;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesListOfUsersMessage;
import com.appspot.powertools_v2.powertools.model.ApiV2MessagesUserMessage;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import com.pt4d.activiti.caches.GroupsByUserCache;
import com.pt4d.activiti.tools.PowertoolsService;
import com.pt4d.config.Pt4dConfig;

public class Pt4dUserManager extends UserEntityManager {
	
	private static Logger log = LoggerFactory.getLogger(Pt4dUserManager.class);
	private static Powertools service = PowertoolsService.createPowertoolsService();
		
	public User createNewUser(String userId) {
		throw new ActivitiException("PT4D user manager doesn't support creating a user");
	}

	public void insertUser(User user) {
		throw new ActivitiException("PT4D user manager doesn't support inserting a user");
	}

	public void updateUser(User updatedUser) {
		throw new ActivitiException("PT4D user manager doesn't support updating a user");
	}

	public User findUserById(String userId) {
		//log.info("User findUserById(String userId)");
		User user = null;
		try {
			ApiV2MessagesUserMessage pt4dUser = service
					.users()
					.get(Pt4dConfig.API_KEY, Pt4dConfig.API_DOMAIN, Pt4dConfig.DEFAULT_USER_ID, userId)
					.execute();
			
			user = new UserEntity(userId);
			user.setEmail(pt4dUser.getEmail());
			user.setFirstName(pt4dUser.getFirstName());
			user.setLastName(pt4dUser.getLastName());
			user.setPassword(Pt4dConfig.USER_PASSWORD);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return user;
	}

	@SuppressWarnings("unchecked")
	public void deleteUser(String userId) {
		throw new ActivitiException("PT4D user manager doesn't support deleting a user");
	}

	@SuppressWarnings("unchecked")
	public List<User> findUserByQueryCriteria(UserQueryImpl query, Page page) {
		List<User> list = new ArrayList<User>();
		
		//log.info("List<User> findUserByQueryCriteria(UserQueryImpl query, Page page)");
		if(query.getId() != null) //log.info(">>> criteria: id " + query.getId());
		if(query.getGroupId() != null) //log.info(">>> criteria: groupId " + query.getGroupId());
		if(query.getFirstName() != null) //log.info(">>> criteria: firstName " + query.getFirstName());
		if(query.getFirstNameLike() != null) //log.info(">>> criteria: firstNameLike " + query.getFirstNameLike());
		if(query.getLastName() != null) //log.info(">>> criteria: lastName " + query.getLastName());
		if(query.getLastNameLike() != null) //log.info(">>> criteria: lastNameLike " + query.getLastNameLike());
		if(query.getFullNameLike() != null) //log.info(">>> criteria: fullNameLike " + query.getFullNameLike());
		if(query.getEmail() != null) //log.info(">>> criteria: email " + query.getEmail());
		if(query.getEmailLike() != null) //log.info(">>> criteria: emailLike " + query.getEmailLike());
		
		if (query.getFirstName() != null || query.getFirstNameLike() != null || query.getLastName() != null
				|| query.getLastNameLike() != null || query.getFullNameLike() != null || query.getEmailLike() != null) {
			throw new ActivitiException("Not implemented user search query criteria"); 
		}
		
		if (query.getId() != null) {
			User user = findUserById(query.getId());
			if (user != null) {
				list.add(user);
			}
		}
		
		if (query.getGroupId() != null) {
			User user = null;
			ApiV2MessagesListOfUsersMessage userList;
			try {
				userList = service.groups().getUsers(Pt4dConfig.API_KEY, Pt4dConfig.API_DOMAIN, Pt4dConfig.DEFAULT_USER_ID, query.getGroupId()).execute();
				
				if (userList != null && userList.getUsers() != null) {
					for(ApiV2MessagesUserMessage pt4dUser : userList.getUsers()) {
						user = new UserEntity(pt4dUser.getId());
						user.setEmail(pt4dUser.getEmail());
						user.setFirstName(pt4dUser.getFirstName());
						user.setLastName(pt4dUser.getLastName());
						user.setPassword(Pt4dConfig.USER_PASSWORD);
						list.add(user);
					}
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (user != null) {
				list.add(user);
			}
		}
		
		if (query.getEmail() != null) {
			User user = null;
			try {
				ApiV2MessagesUserMessage pt4dUser = service
						.users()
						.get(Pt4dConfig.API_KEY, Pt4dConfig.API_DOMAIN, Pt4dConfig.DEFAULT_USER_ID, query.getEmail())
						.execute();
				
				user = new UserEntity(pt4dUser.getId());
				user.setEmail(pt4dUser.getEmail());
				user.setFirstName(pt4dUser.getFirstName());
				user.setLastName(pt4dUser.getLastName());
				user.setPassword(Pt4dConfig.USER_PASSWORD);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (user != null) {
				list.add(user);
			}
		}

		//log.info(">>> " + list.size() + " users found");
		return list;
	}

	public long findUserCountByQueryCriteria(UserQueryImpl query) {
		//log.info("long findUserCountByQueryCriteria(UserQueryImpl query)");
		return super.findUserCountByQueryCriteria(query);
	}

	@SuppressWarnings("unchecked")
	public List<Group> findGroupsByUser(String userId) {
		////log.info("List<Group> findGroupsByUser(String userId)");
		////log.info(">>> userId " + userId);
		List<Group> list = new ArrayList<Group>();
		
		ApiV2MessagesListOfGroupsMessage groupList = GroupsByUserCache.getInstance().get(userId);
		try {
			if (groupList == null) {
				log.info("List<Group> findGroupsByUser(String userId) => No cache found, calling pt4d api ...");
				groupList = service.users()
						.getGroups(Pt4dConfig.API_KEY, Pt4dConfig.API_DOMAIN, Pt4dConfig.DEFAULT_USER_ID, userId).execute();
				GroupsByUserCache.getInstance().put(userId, groupList);
			}
			
			if (groupList != null && groupList.getGroups() != null ) {
				for (ApiV2MessagesGroupMessage group : groupList.getGroups()) {
					Group g = new GroupEntity(group.getId());
					g.setName(group.getEmail());
					list.add(g);
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		////log.info(">>> " + list.size() + " groups found");
		return list;
	}

	public UserQuery createNewUserQuery() {
		return super.createNewUserQuery();
	}

	public IdentityInfoEntity findUserInfoByUserIdAndKey(String userId, String key) {
		return super.findUserInfoByUserIdAndKey(userId, key);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<String> findUserInfoKeysByUserIdAndType(String userId, String type) {
		//log.info("List<String> findUserInfoKeysByUserIdAndType(String userId, String type)");
		return findUserInfoKeysByUserIdAndType(userId, type);
	}

	public Boolean checkPassword(String userId, String password) {
		if(Pt4dConfig.USER_PASSWORD.equals(password)) {
			return true;
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	public List<User> findPotentialStarterUsers(String proceDefId) {
		throw new ActivitiException("PT4D user manager doesn't support querying");
	}

	@SuppressWarnings("unchecked")
	public List<User> findUsersByNativeQuery(Map<String, Object> parameterMap, int firstResult, int maxResults) {
		throw new ActivitiException("PT4D user manager doesn't support querying");
	}

	public long findUserCountByNativeQuery(Map<String, Object> parameterMap) {
		throw new ActivitiException("PT4D user manager doesn't support querying");
	}

	@Override
	public boolean isNewUser(User user) {
		throw new ActivitiException("PT4D user manager doesn't support updating a user");
	}

	@Override
	public Picture getUserPicture(String userId) {
		return null;
	}

	@Override
	public void setUserPicture(String userId, Picture picture) {
		throw new ActivitiException("PT4D user manager doesn't support updating a user");
	}

}
