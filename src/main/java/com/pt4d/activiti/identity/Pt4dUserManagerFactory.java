/**
 * 
 */
package com.pt4d.activiti.identity;

import org.activiti.engine.impl.interceptor.Session;
import org.activiti.engine.impl.interceptor.SessionFactory;
import org.activiti.engine.impl.persistence.entity.GroupIdentityManager;
import org.activiti.engine.impl.persistence.entity.UserIdentityManager;

/**
 * @author stephane
 *
 */
public class Pt4dUserManagerFactory implements SessionFactory {

	@Override
	public Class<?> getSessionType() {
		return UserIdentityManager.class;
	}

	@Override
	public Session openSession() {
		return new Pt4dUserManager();
	}

}
