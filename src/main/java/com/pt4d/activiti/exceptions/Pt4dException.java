package com.pt4d.activiti.exceptions;

public class Pt4dException extends Exception {
    public Pt4dException(String message) {
        super(message);
    }
}