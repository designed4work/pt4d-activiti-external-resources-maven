/*
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
/*
 * This code was generated by https://github.com/google/apis-client-generator/
 * (build: 2017-11-07 19:12:12 UTC)
 * on 2017-12-19 at 10:34:35 UTC 
 * Modify at your own risk.
 */

package com.appspot.powertools_v2.powertools.model;

/**
 * Model definition for ApiV2MessagesFileMessage.
 *
 * <p> This is the Java data model class that specifies how to parse/serialize into the JSON that is
 * transmitted over HTTP when working with the powertools. For a detailed explanation see:
 * <a href="https://developers.google.com/api-client-library/java/google-http-java-client/json">https://developers.google.com/api-client-library/java/google-http-java-client/json</a>
 * </p>
 *
 * @author Google, Inc.
 */
@SuppressWarnings("javadoc")
public final class ApiV2MessagesFileMessage extends com.google.api.client.json.GenericJson {

  /**
   * The value may be {@code null}.
   */
  @com.google.api.client.util.Key
  private java.util.List<ApiV2MessagesFileAttributeMessage> attributes;

  /**
   * The value may be {@code null}.
   */
  @com.google.api.client.util.Key
  private ApiV2MessagesUserMessage creator;

  /**
   * The value may be {@code null}.
   */
  @com.google.api.client.util.Key
  private java.lang.String driveData;

  /**
   * The value may be {@code null}.
   */
  @com.google.api.client.util.Key
  private ApiV2MessagesFileTypeMessage fileType;

  /**
   * The value may be {@code null}.
   */
  @com.google.api.client.util.Key
  private java.lang.String id;

  /**
   * The value may be {@code null}.
   */
  @com.google.api.client.util.Key
  private java.lang.Boolean isActive;

  /**
   * The value may be {@code null}.
   */
  @com.google.api.client.util.Key
  private java.lang.Boolean isDeleted;

  /**
   * The value may be {@code null}.
   */
  @com.google.api.client.util.Key
  private java.lang.Boolean isOut;

  /**
   * The value may be {@code null}.
   */
  @com.google.api.client.util.Key
  private java.lang.Boolean isTechnical;

  /**
   * The value may be {@code null}.
   */
  @com.google.api.client.util.Key
  private java.lang.Boolean isTrashed;

  /**
   * The value may be {@code null}.
   */
  @com.google.api.client.util.Key
  private java.lang.String kind;

  /**
   * The value may be {@code null}.
   */
  @com.google.api.client.util.Key
  private ApiV2MessagesUserMessage owner;

  /**
   * The value may be {@code null}.
   */
  @com.google.api.client.util.Key
  private java.lang.String parentFileId;

  /**
   * The value may be {@code null}.
   */
  @com.google.api.client.util.Key
  private ApiV2MessagesUserMessage publisher;

  /**
   * The value may be {@code null}.
   */
  @com.google.api.client.util.Key
  private java.lang.String status;

  /**
   * The value may be {@code null}.
   */
  @com.google.api.client.util.Key
  private java.util.List<ApiV2MessagesTagMessage> tags;

  static {
    // hack to force ProGuard to consider ApiV2MessagesTagMessage used, since otherwise it would be stripped out
    // see https://github.com/google/google-api-java-client/issues/543
    com.google.api.client.util.Data.nullOf(ApiV2MessagesTagMessage.class);
  }

  /**
   * The value may be {@code null}.
   */
  @com.google.api.client.util.Key @com.google.api.client.json.JsonString
  private java.lang.Long versionNumber;

  /**
   * The value may be {@code null}.
   */
  @com.google.api.client.util.Key
  private ApiV2MessagesWorkspaceMessage workspace;

  /**
   * @return value or {@code null} for none
   */
  public java.util.List<ApiV2MessagesFileAttributeMessage> getAttributes() {
    return attributes;
  }

  /**
   * @param attributes attributes or {@code null} for none
   */
  public ApiV2MessagesFileMessage setAttributes(java.util.List<ApiV2MessagesFileAttributeMessage> attributes) {
    this.attributes = attributes;
    return this;
  }

  /**
   * @return value or {@code null} for none
   */
  public ApiV2MessagesUserMessage getCreator() {
    return creator;
  }

  /**
   * @param creator creator or {@code null} for none
   */
  public ApiV2MessagesFileMessage setCreator(ApiV2MessagesUserMessage creator) {
    this.creator = creator;
    return this;
  }

  /**
   * @return value or {@code null} for none
   */
  public java.lang.String getDriveData() {
    return driveData;
  }

  /**
   * @param driveData driveData or {@code null} for none
   */
  public ApiV2MessagesFileMessage setDriveData(java.lang.String driveData) {
    this.driveData = driveData;
    return this;
  }

  /**
   * @return value or {@code null} for none
   */
  public ApiV2MessagesFileTypeMessage getFileType() {
    return fileType;
  }

  /**
   * @param fileType fileType or {@code null} for none
   */
  public ApiV2MessagesFileMessage setFileType(ApiV2MessagesFileTypeMessage fileType) {
    this.fileType = fileType;
    return this;
  }

  /**
   * @return value or {@code null} for none
   */
  public java.lang.String getId() {
    return id;
  }

  /**
   * @param id id or {@code null} for none
   */
  public ApiV2MessagesFileMessage setId(java.lang.String id) {
    this.id = id;
    return this;
  }

  /**
   * @return value or {@code null} for none
   */
  public java.lang.Boolean getIsActive() {
    return isActive;
  }

  /**
   * @param isActive isActive or {@code null} for none
   */
  public ApiV2MessagesFileMessage setIsActive(java.lang.Boolean isActive) {
    this.isActive = isActive;
    return this;
  }

  /**
   * @return value or {@code null} for none
   */
  public java.lang.Boolean getIsDeleted() {
    return isDeleted;
  }

  /**
   * @param isDeleted isDeleted or {@code null} for none
   */
  public ApiV2MessagesFileMessage setIsDeleted(java.lang.Boolean isDeleted) {
    this.isDeleted = isDeleted;
    return this;
  }

  /**
   * @return value or {@code null} for none
   */
  public java.lang.Boolean getIsOut() {
    return isOut;
  }

  /**
   * @param isOut isOut or {@code null} for none
   */
  public ApiV2MessagesFileMessage setIsOut(java.lang.Boolean isOut) {
    this.isOut = isOut;
    return this;
  }

  /**
   * @return value or {@code null} for none
   */
  public java.lang.Boolean getIsTechnical() {
    return isTechnical;
  }

  /**
   * @param isTechnical isTechnical or {@code null} for none
   */
  public ApiV2MessagesFileMessage setIsTechnical(java.lang.Boolean isTechnical) {
    this.isTechnical = isTechnical;
    return this;
  }

  /**
   * @return value or {@code null} for none
   */
  public java.lang.Boolean getIsTrashed() {
    return isTrashed;
  }

  /**
   * @param isTrashed isTrashed or {@code null} for none
   */
  public ApiV2MessagesFileMessage setIsTrashed(java.lang.Boolean isTrashed) {
    this.isTrashed = isTrashed;
    return this;
  }

  /**
   * @return value or {@code null} for none
   */
  public java.lang.String getKind() {
    return kind;
  }

  /**
   * @param kind kind or {@code null} for none
   */
  public ApiV2MessagesFileMessage setKind(java.lang.String kind) {
    this.kind = kind;
    return this;
  }

  /**
   * @return value or {@code null} for none
   */
  public ApiV2MessagesUserMessage getOwner() {
    return owner;
  }

  /**
   * @param owner owner or {@code null} for none
   */
  public ApiV2MessagesFileMessage setOwner(ApiV2MessagesUserMessage owner) {
    this.owner = owner;
    return this;
  }

  /**
   * @return value or {@code null} for none
   */
  public java.lang.String getParentFileId() {
    return parentFileId;
  }

  /**
   * @param parentFileId parentFileId or {@code null} for none
   */
  public ApiV2MessagesFileMessage setParentFileId(java.lang.String parentFileId) {
    this.parentFileId = parentFileId;
    return this;
  }

  /**
   * @return value or {@code null} for none
   */
  public ApiV2MessagesUserMessage getPublisher() {
    return publisher;
  }

  /**
   * @param publisher publisher or {@code null} for none
   */
  public ApiV2MessagesFileMessage setPublisher(ApiV2MessagesUserMessage publisher) {
    this.publisher = publisher;
    return this;
  }

  /**
   * @return value or {@code null} for none
   */
  public java.lang.String getStatus() {
    return status;
  }

  /**
   * @param status status or {@code null} for none
   */
  public ApiV2MessagesFileMessage setStatus(java.lang.String status) {
    this.status = status;
    return this;
  }

  /**
   * @return value or {@code null} for none
   */
  public java.util.List<ApiV2MessagesTagMessage> getTags() {
    return tags;
  }

  /**
   * @param tags tags or {@code null} for none
   */
  public ApiV2MessagesFileMessage setTags(java.util.List<ApiV2MessagesTagMessage> tags) {
    this.tags = tags;
    return this;
  }

  /**
   * @return value or {@code null} for none
   */
  public java.lang.Long getVersionNumber() {
    return versionNumber;
  }

  /**
   * @param versionNumber versionNumber or {@code null} for none
   */
  public ApiV2MessagesFileMessage setVersionNumber(java.lang.Long versionNumber) {
    this.versionNumber = versionNumber;
    return this;
  }

  /**
   * @return value or {@code null} for none
   */
  public ApiV2MessagesWorkspaceMessage getWorkspace() {
    return workspace;
  }

  /**
   * @param workspace workspace or {@code null} for none
   */
  public ApiV2MessagesFileMessage setWorkspace(ApiV2MessagesWorkspaceMessage workspace) {
    this.workspace = workspace;
    return this;
  }

  @Override
  public ApiV2MessagesFileMessage set(String fieldName, Object value) {
    return (ApiV2MessagesFileMessage) super.set(fieldName, value);
  }

  @Override
  public ApiV2MessagesFileMessage clone() {
    return (ApiV2MessagesFileMessage) super.clone();
  }

}
