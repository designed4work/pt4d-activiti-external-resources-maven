/*
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
/*
 * This code was generated by https://github.com/google/apis-client-generator/
 * (build: 2017-11-07 19:12:12 UTC)
 * on 2017-12-19 at 10:34:35 UTC 
 * Modify at your own risk.
 */

package com.appspot.powertools_v2.powertools.model;

/**
 * Model definition for ApiV2MessagesListOfTagsMessage.
 *
 * <p> This is the Java data model class that specifies how to parse/serialize into the JSON that is
 * transmitted over HTTP when working with the powertools. For a detailed explanation see:
 * <a href="https://developers.google.com/api-client-library/java/google-http-java-client/json">https://developers.google.com/api-client-library/java/google-http-java-client/json</a>
 * </p>
 *
 * @author Google, Inc.
 */
@SuppressWarnings("javadoc")
public final class ApiV2MessagesListOfTagsMessage extends com.google.api.client.json.GenericJson {

  /**
   * The value may be {@code null}.
   */
  @com.google.api.client.util.Key
  private java.util.List<ApiV2MessagesTagMessage> tags;

  /**
   * @return value or {@code null} for none
   */
  public java.util.List<ApiV2MessagesTagMessage> getTags() {
    return tags;
  }

  /**
   * @param tags tags or {@code null} for none
   */
  public ApiV2MessagesListOfTagsMessage setTags(java.util.List<ApiV2MessagesTagMessage> tags) {
    this.tags = tags;
    return this;
  }

  @Override
  public ApiV2MessagesListOfTagsMessage set(String fieldName, Object value) {
    return (ApiV2MessagesListOfTagsMessage) super.set(fieldName, value);
  }

  @Override
  public ApiV2MessagesListOfTagsMessage clone() {
    return (ApiV2MessagesListOfTagsMessage) super.clone();
  }

}
