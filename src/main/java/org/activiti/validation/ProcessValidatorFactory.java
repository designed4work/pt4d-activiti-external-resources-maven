/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.activiti.validation;

import org.activiti.rest.service.api.RestResponseFactory;
import org.activiti.validation.validator.ValidatorSet;
import org.activiti.validation.validator.ValidatorSetFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.pt4d.activiti.validation.DefaultPt4dElementValidator;

/**
 * @author jbarrez
 */
public class ProcessValidatorFactory {
	
	private static Logger log = LoggerFactory.getLogger(ProcessValidatorFactory.class);
	
	public ProcessValidator createDefaultProcessValidator() {
		ProcessValidatorImpl processValidator = new ProcessValidatorImpl();
		processValidator.addValidatorSet(new ValidatorSetFactory().createActivitiExecutableProcessValidatorSet());
		
		//Custom validation
		log.info("Adding PT4D Validator set");
		ValidatorSet customValidatorSet = new ValidatorSet("PT4D ValidatorSet"); 
		customValidatorSet.addValidator(new DefaultPt4dElementValidator());
		processValidator.addValidatorSet(customValidatorSet);
		
		return processValidator;
	}

}